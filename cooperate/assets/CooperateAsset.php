<?php

namespace cooperate\assets;

use yii\web\AssetBundle;


class CooperateAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/w3.css',
        'css/site.css',
        'css/font-awesome-4.7.0/css/font-awesome.css',
        'css/cooperate.css',
    ];
    public $js = [
        'js/cooperate.js',
        'js/jquery.min-3.2.0.js',
        'js/bootstrap.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}