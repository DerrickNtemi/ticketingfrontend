$(".navbar-toggle").click(function () {
    if ($(this).val() === "1") {
        $(this).val(0);
        $(".resizableHeight").css("minHeight", 120);
    }else{
        $(this).val(1);    
        $(".resizableHeight").css("minHeight", 0);
    }
});