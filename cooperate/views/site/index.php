<?php

use yii\helpers\Html;

$this->title = 'Home';
?>
<!-- Slide show -->
<div class="row slideshow-row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 slideshow-row-div">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'mySlides']) ?>
                        </div>
                        <div class="item">
                            <?= Html::img("@web/images/slideshows/slide1.jpg", ['class' => 'mySlides']) ?>
                        </div>

                        <div class="item">
                            <?= Html::img("@web/images/slideshows/slide2.jpg", ['class' => 'mySlides']) ?>
                        </div>

                        <div class="item">
                            <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'mySlides']) ?>
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Index Page Contents -->
<div class="row homepage-content">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <!-- Search Box -->
        <div class="row">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            <div class="col-xs-8 col-sm-8 col-md-10 col-lg-10">
                <div class="row event-search-card">
                    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                        <input type="text" name="event" />
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                        <button id="search-button">search <span class="glyphicon glyphicon-search"></span></button>
                    </div>
                </div>
            </div>
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
        </div>
        <!-- Upcoming Events Cards -->
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row upcoming-events-title-row">
                    <h3 class="upcoming-events-title">Upcoming Events</h3>
                </div>
                <div class="row events-list-row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row events-list-row">
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row event-cards-rows">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-holder" >
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <?= Html::img("@web/images/slideshows/slide.jpg", ['class' => 'event-poster']) ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-card-details">
                                        <h6 class="events-date">WED, JUN 28 8:00 AM</h6>
                                        <h4 class="events-title">Ndakaini Half marathon</h4>
                                        <h5 class="events-venue">Ndakaini, Nairobi, Kenya</h5>
                                    </div>
                                </div>
                                <div class="row events-social-media-row">
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 event-card-details">
                                        <h6 class="events-social-media">#NdakainiHalfMarathon</h4>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-card-details-link text-right">
                                        <a href=''><span class="glyphicon glyphicon-eye-open"></span></a>
                                        <a href=''><span class="glyphicon glyphicon-shopping-cart"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
