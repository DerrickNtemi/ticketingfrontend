<?php

use cooperate\assets\CooperateAsset;
use yii\helpers\Html;

$identity = Yii::$app->user->getIdentity();
CooperateAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <style>
            html, body, #main-container{
                background-color: #d9d9d9; /* For browsers that do not support gradients */
                background-image: url('./../images/ptn.png');
                background-position: left top;
                background-size: auto;
                background-repeat: no-repeat;
                background-attachment: fixed;
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="container" id="main-container">
            <div class="row main-content-row">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 main-content">
                    <div class="row">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-10">
                            <div class="row header-shade-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>                                 
                            </div>
                            <div class="row header-row resizableHeight">
                                <div class="col-xs-7 col-sm-7 col-md-4 col-lg-4 logo-div">
                                    <?= Html::img("@web/images/logo.png", ['class' => 'img-responsive']) ?>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-8 col-lg-8 menu-div ">
                                    <nav class="navbar navbar-inverse" id="navigation-bar">
                                        <div class="container-fluid">
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" value="1" data-toggle="collapse" data-target="#myNavbar" aria-expanded="true">
                                                    <span class="icon-bar"></span> 
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>                        
                                                </button>
                                            </div>
                                            <div class="collapse navbar-collapse" id="myNavbar" aria-expanded="true">
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="#">Home</a></li>
                                                    <li><a href="#">Events</a></li>
                                                    <li><a href="#">Contact</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <?= $content ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                    </div>
                    <div class="row footer-row-social">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            <span class="fa fa-facebook"></span>
                            <span class="fa fa-twitter"></span>
                            <span class="fa fa-youtube"></span>
                            <span class="fa fa-google-plus"></span>
                            <span class="fa fa-linkedin"></span>
                        </div>
                    </div>
                    <div class="row footer-row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <p>&copy; <?=date("Y")?> Jambopay Ticketing</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
