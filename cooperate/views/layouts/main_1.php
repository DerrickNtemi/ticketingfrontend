<?php

use cooperate\assets\CooperateAsset;
use yii\helpers\Html;

$identity = Yii::$app->user->getIdentity();
CooperateAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <style>
            html, body, #main-container{
                background-color: #d9d9d9; /* For browsers that do not support gradients */
                background-image: url('./../images/ptn.png');
                background-position: left top;
                background-size: auto;
                background-repeat: no-repeat;
                background-attachment: fixed;
            }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="container" id="main-container">
            <div class="row main-content-row">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 main-content">
                    <div class="row">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div class="row header-shade-row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>                                 
                            </div>
                            <div class="row header-row">
                                <div class="col-xs-8 col-sm-8 col-md-4 col-lg-4 logo-div">
                                    <?= Html::img("@web/images/logo.png", ['class' => 'img-responsive']) ?>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-8 col-lg-8 menu-div">
                                    <nav class="navbar navbar-inverse">
                                        <div class="container-fluid">
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>                        
                                                </button>
                                            </div>
                                            <div class="collapse navbar-collapse" id="myNavbar">
                                                <ul class="nav navbar-nav">
                                                    <li class="active"><a href="#">Home</a></li>
                                                    <li class="dropdown">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#">Page 1-1</a></li>
                                                            <li><a href="#">Page 1-2</a></li>
                                                            <li><a href="#">Page 1-3</a></li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Page 2</a></li>
                                                    <li><a href="#">Page 3</a></li>
                                                </ul>
                                                <ul class="nav navbar-nav navbar-right">
                                                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                                                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                    </div>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
