<?php

namespace merchants\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class MerchantsAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/merchant.css',
    ];
    public $js = [
        'js/merchant.js',
        'js/bootstrap-wizard.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}