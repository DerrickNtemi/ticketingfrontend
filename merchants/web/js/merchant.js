var errorMessage = 'ERROR CODE 400 :: An error occurred while processing your request, please try again or contact system administrator for assistance..';
var errorNotFound = "ERROR CODE 404 :: The requested resource was not found";
var errorInternalServer = "ERROR CODE 500 :: Internal server error, please try again or contact system administrator for assistance..";

$(document).ready(function () {
    $('#loadingDiv').hide();
    $('#rootwizard').bootstrapWizard({
        onNext: function (tab, navigation, index) {
            var total = (navigation.find('li').length) - 1;
            if (index === 1) {
                var form = $('#events-details-form');
                var url = form.attr('action');
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#events-details-error-summary");
                return false;
            } else if (index === 2) {
                var eventsId = $("#events-id").val();
                var form = $('#events-details-form');
                var url = form.attr('action') + "-checktickets?eventsId=" + eventsId;
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#ticket-details-error-summary");
                return false;
            } else if (index === 4) {
                var eventsId = $("#events-id").val();
                var form = $('#events-details-form');
                var url = form.attr('action') + "-finish-display?eventsId=" + eventsId;
                var data = new FormData($(form)[0]);
                silentWizardCommunication(url, data, "#rootwizard", index, total, "#event-gallery-error-summary");
                return false;
            }
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizard').find('.bar').css({width: percent + '%'});
            $('#rootwizard').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                //$('#rootwizard').find('.pager .previous').addClass('disabled');
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
            return false;
        }
    });

    $('#rootwizardupdate').bootstrapWizard({
        onNext: function (tab, navigation, index) {
        },
        onTabShow: function (tab, navigation, index) {
            var total = (navigation.find('li').length);
            var percent = ((index + 1) / total) * 100;
            $('#rootwizardupdate').find('.bar').css({width: percent + '%'});
            $('#rootwizardupdate').find('.bar').text(Math.round(percent) + '% Complete');
            // If it's the last tab then hide the last button and show the finish instead
            if ((index + 1) >= total) {
                //$('#rootwizard').find('.pager .previous').addClass('disabled');
                $('#rootwizardupdate').find('.pager .next').hide();
                $('#rootwizardupdate').find('.pager .finish').show();
                $('#rootwizardupdate').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizardupdate').find('.pager .next').show();
                $('#rootwizardupdate').find('.pager .finish').hide();
            }
        },
        onTabClick: function (tab, navigation, index) {
        }
    });

    $('#update-event-details').click(function () {
        var form = $('#events-details-form');
        var url = form.attr('action') + "-update";
        var data = new FormData($(form)[0]);
        silentWizardRedirectComm(url, data, "#events-details-error-summary");
    });

    $('#rootwizard .finish').click(function () {
        var form = $('#events-details-form');
        var data = new FormData(form[0]);
        var eventsId = $("#events-id").val();
        var url = form.attr('action') + "-finish?eventsId=" + eventsId;
        silentWizardRedirectComm(url, data, "#finish-error-summary");
    });

    $('#TicketTierModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        modal.find('#error-summary').hide();
        var data = button.data('whatever');
        if (data !== "" && data !== undefined) {
            modal.find('#tickettier-btn').text("Update");
            modal.find('#tickettier-modal-header').text("Update Ticket Tier");
            modal.find('.modal-body #eventtickets-id').val(data.id);
            modal.find('.modal-body #eventtickets-eventid').val(data.eventId.id);
            modal.find('.modal-body #eventtickets-tickettypeid').val(data.ticketTypeId.id);
            modal.find('.modal-body #eventtickets-ticketnameid').val(data.ticketNameId);
            modal.find('.modal-body #eventtickets-price').val(data.price);
            modal.find('.modal-body #eventtickets-ticketquantity').val(data.ticketQuantity);
            modal.find('.modal-body #eventtickets-purchasestartdatetime').val(data.purchaseStartDateTime);
            modal.find('.modal-body #eventtickets-purchaseenddatetime').val(data.purchaseEndDateTime);
        } else if (data !== undefined) {
            modal.find('#tickettier-btn').text("Create");
            modal.find('#tickettier-modal-header').text("Create Ticket Tier");
            modal.find('.modal-body #eventtickets-id').val(0);
            modal.find('.modal-body #eventtickets-tickettypeid').val(0);
            modal.find('.modal-body #eventtickets-ticketnameid').val(0);
            modal.find('.modal-body #eventtickets-price').val("");
            modal.find('.modal-body #eventtickets-ticketquantity').val("");
            modal.find('.modal-body #eventtickets-purchasestartdatetime').val("");
            modal.find('.modal-body #eventtickets-purchaseenddatetime').val("");
        }
    });

    $('#tickettier-btn').on('click', function () {
        var form = $('#ticket-tier-form');
        var url = form.attr('action');
        $('#eventtickets-eventsid').val($("#events-id").val());
        var data = new FormData(form[0]);
        var successId = "#ticket-details-success-message";
        var modalId = "#TicketTierModal";
        var reloadDiv = "#ticket-tier-details";
        silentReloadCommunication(url, data, modalId, successId, reloadDiv);
    });

    $('#delete-inner-reload-btn').on('click', function () {
        var modalId = "#DeleteInnerReloadModal";
        var modal = $(modalId);
        var reloadDiv = modal.find('.modal-body #reload-div').text();
        var successDiv = modal.find('.modal-body #success-div').text();
        var url = modal.find('.modal-body #delete-url').text();
        var data = "";
        silentReloadCommunication(url, data, modalId, successDiv, reloadDiv);
    });

    $('#update-ticket-tier-reload-btn').on('click', function () {
        var modalId = "#ChangeTicketUnitsModal";
        var modal = $(modalId);
        var reloadDiv = modal.find('.modal-body #reload-div').text();
        var successDiv = modal.find('.modal-body #success-div').text();
        var ticketunits = modal.find('.modal-body #ticketunits').val();
        var url = modal.find('.modal-body #change-url').text() + "&units=" + ticketunits;
        var data = "";
        silentReloadCommunication(url, data, modalId, successDiv, reloadDiv);
    });


    $('#create-event-sponsors-btn').on('click', function () {
        var form = $('#event-sponsor-form');
        var url = form.attr('action');
        $('#eventsponsor-eventid').val($("#events-id").val());
        var data = new FormData(form[0]);
        var successId = "#event-sponsor-success-message";
        var modalId = "#EventSponsorsModal";
        var reloadDiv = "#event-sponsors-details";
        silentReloadCommunication(url, data, modalId, successId, reloadDiv);
    });


    $('#create-event-gallery-btn').on('click', function () {
        var form = $('#event-gallery-form');
        var url = form.attr('action');
        $('#eventgallery-eventid').val($("#events-id").val());
        var data = new FormData(form[0]);
        var successId = "#event-gallery-success-message";
        var modalId = "#EventGalleryModal";
        var reloadDiv = "#event-gallery-details";
        silentReloadCommunication(url, data, modalId, successId, reloadDiv);
    });
    
    $('#EventSponsorsModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        if (data !== "") {
            modal.find('#event-sponsor-modal-header').text("Update record");
            modal.find('#create-event-sponsors-btn').text("Update");
            modal.find('.modal-body #eventsponsor-id').val(data.id);
            modal.find('.modal-body #eventsponsor-name').val(data.name);
            modal.find('.modal-body #eventsponsor-itemsponsoring').val(data.itemSponsoring);
            modal.find('.modal-body #eventsponsor-businessurl').val(data.businessUrl);
        } else {
            modal.find('#event-sponsor-modal-header').text("Create record");
            modal.find('#create-event-sponsors-btn').text("Create");
            modal.find('.modal-body #eventsponsor-id').val('');
            modal.find('.modal-body #eventsponsor-name').val('');
            modal.find('.modal-body #eventsponsor-itemsponsoring').val('');
            modal.find('.modal-body #eventsponsor-businessurl').val('');
        }
    });
    
    $('#EventGalleryModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        if (data !== "") {
            modal.find('#event-gallery-modal-header').text("Update record");
            modal.find('#create-event-gallery-btn').text("Update");
            modal.find('.modal-body #eventgallery-id').val(data.id);
            modal.find('.modal-body #eventgallery-title').val(data.title);
            modal.find('.modal-body #eventgallery-description').val(data.description);
        } else {
            modal.find('#event-gallery-modal-header').text("Create record");
            modal.find('#create-event-gallery-btn').text("Create");
            modal.find('.modal-body #eventgallery-id').val('');
            modal.find('.modal-body #eventgallery-title').val('');
            modal.find('.modal-body #eventgallery-description').val('');
        }
    });

    $('#ChangeTicketUnitsModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var div = button.data('div');
        var successDiv = button.data('value');
        modal.find('.modal-body #reload-div').text(div);
        modal.find('.modal-body #success-div').text(successDiv);
        modal.find('.modal-body #change-url').text(data);
        modal.find('.modal-body #form-processing-loader').hide();
        modal.find('.modal-body #error-summary').hide('');
        modal.find('.modal-body #ticketunits').val('');
    });


    $('#DeleteInnerReloadModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var data = button.data('whatever');
        var div = button.data('div');
        var successDiv = button.data('value');
        modal.find('.modal-body #reload-div').text(div);
        modal.find('.modal-body #success-div').text(successDiv);
        modal.find('.modal-body #delete-url').text(data);
        modal.find('.modal-body #form-processing-loader').hide();
        modal.find('.modal-body #error-summary').hide('');
    });


    function silentWizardRedirectComm(url, data, errorDiv, button) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorDiv).hide();
                $(button).attr('disabled', true);
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    window.location = data.success.url;
                    $(button).attr('disabled', false);
                } else {
                    $(errorDiv).show().html(data.error);
                    $(button).attr('disabled', false);
                }
            },
            error: function () {
                $(errorDiv).show().html(errorMessage);
                $(button).attr('disabled', false);
            },
            statusCode: {
                404: function () {
                    $(errorDiv).show().html(errorNotFound);
                    $(button).attr('disabled', false);
                },
                500: function () {
                    $(errorDiv).show().html(errorInternalServer);
                    $(button).attr('disabled', false);
                }
            }
        });
    }

    $('#delete-record').on('click', function () {
        var deleteModal = $('#DeleteRecordModal');
        var url = deleteModal.find('.modal-body #url').text();
        var reloadDiv = deleteModal.find('.modal-body #reload-div').text();
        $.ajax({
            url: url,
            type: 'POST',
            beforeSend: function () {
                deleteModal.find('.modal-body #progress-spinner').show();
                deleteModal.find('.modal-body #error-summary').hide();
            },
            complete: function () {
                deleteModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    deleteModal.modal('hide');
                    deleteModal.removeData('modal');
                    $(reloadDiv).html(data.success);
                } else {
                    deleteModal.find('.modal-body #error-summary').show().html(data.error);
                }
            },
            error: function () {
                deleteModal.find('.modal-body #error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorNotFound);
                },
                500: function () {
                    deleteModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                }
            }
        });
    });


    function silentCommunicationWithRedirect(activeModalId, submitForm, button) {
        var activeModal = $(activeModalId);
        var form = $(submitForm);
        var formData = new FormData(form[0]);
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('.modal-body #progress-spinner').show();
                activeModal.find('.modal-body #error-summary').hide();
                button.attr('disabled', true);
            },
            complete: function () {
                activeModal.find('.modal-body #progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    activeModal.modal('hide');
                    activeModal.removeData('modal');
                    window.location = data.success;
                    button.attr('disabled', false);
                } else {
                    activeModal.find('.modal-body #error-summary').show().html(data.error);
                    button.attr('disabled', false);
                }
            },
            error: function () {
                activeModal.find('.modal-body #error-summary').show().html(errorMessage);
                button.attr('disabled', false);
            },
            statusCode: {
                404: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorNotFound);
                    button.attr('disabled', false);
                },
                500: function () {
                    activeModal.find('.modal-body #error-summary').show().html(errorInternalServer);
                    button.attr('disabled', false);
                }
            }
        });
    }

    function silentWizardCommunication(url, data, wizard, index, total, errorId) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                $('#loadingDiv').show();
                $(errorId).hide();
            },
            complete: function () {
                $('#loadingDiv').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    if (wizard === "#rootwizard" && index === 1) {
                        $("#events-id").val(data.success);
                    }
                    if (index === total && wizard === "#rootwizard") {
                        $('#tab' + (index + 1)).html(data.success);
                    }
                    $(wizard).bootstrapWizard('show', index);
                    $(errorId).hide();
                } else {
                    $(errorId).show().html(data.error);
                }
            },
            error: function () {
                $(errorId).show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    $(errorId).show().html(errorNotFound);
                },
                500: function () {
                    $(errorId).show().html(errorInternalServer);
                }
            }
        });
    }

    function silentReloadCommunication(url, data, modalId, successId, reloadDiv) {
        var activeModal = $(modalId);
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
                activeModal.find('#progress-spinner').show();
                activeModal.find('#error-summary').hide();
            },
            complete: function () {
                activeModal.find('#progress-spinner').hide();
            },
            success: function (data) {
                if (typeof data.success !== 'undefined') {
                    console.log(data.success.results);
                    activeModal.modal('hide');
                    $(reloadDiv).html(data.success.results);
                    $(successId).show().html(data.success.message);
                } else {
                    activeModal.find('#error-summary').show().html(data.error);
                }
            },
            error: function () {
                activeModal.find('#error-summary').show().html(errorMessage);
            },
            statusCode: {
                404: function () {
                    activeModal.find('#error-summary').show().html(errorNotFound);
                },
                500: function () {
                    activeModal.find('#error-summary').show().html(errorInternalServer);
                }
            }
        });
    }

});

window.setInterval(resizeResizeableHeight, 200);
function resizeResizeableHeight() {
    $('.resizableHeight').each(function () {
        $(this).css("minHeight", $(this).parent().height() - ($(this).offset().top - ($(this).parent().offset().top + parseInt($(this).parent().css('padding-top')))));
    });
}