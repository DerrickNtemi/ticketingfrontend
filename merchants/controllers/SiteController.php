<?php

namespace merchants\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Service;
use common\models\ResetPassword;
use common\models\ResetPasswordRequest;
use common\models\Merchants;
use common\models\ChangePassword;
use yii\helpers\Html;

/**
 * Site controller
 */
class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'error', 'request-password-reset', 'reset-password', 'info', 'activate-account', 'account-creation'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $appName = "Merchant";
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('@common/views/login', [
                        'model' => $model,
                        'appName' => $appName,
            ]);
        }
    }

    public function actionAccountCreation() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $service = new Service();
        $countriesData = $service->getCountry();
        $countries = [];
        foreach ($countriesData as $data) {
            $countries[$data['id']] = $data['name'];
        }
        $appName = "Merchant";
        $model = new Merchants();
        if ($model->load(Yii::$app->request->post())) {
            $response = (Object) $service->merchantAccountCreation($model);
            if ($response->status['code'] == 100) {
                $url = Html::a("Back to login", ['/site/login'], ['class' => 'btn btn-small btn-success']);
                \Yii::$app->getSession()->setFlash('success-message', "Account created sucessfully. Check email for activation details " . $url);
                return $this->redirect(['info']);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('accountcreation', [
                    'model' => $model,
                    'appName' => $appName,
                    'countries' => $countries,
        ]);
    }

    public function actionActivateAccount($token) {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $service = new Service();
        $response = (Object) $service->activateAccount($token);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Verification was successful. Reset password");
            Yii::$app->session->set('credentialId', $response->item);
            return $this->redirect('reset-password');
        } else {
            $url = Html::a("Back to login", ['/site/login'], ['class' => 'btn btn-small btn-danger']);
            \Yii::$app->getSession()->setFlash('error-message', $response->status['message'] . ". Please conduct system administrator for further assistance....." . $url);
            return $this->redirect(['info']);
        }
    }

    public function actionRequestPasswordReset() {
        $model = new ResetPasswordRequest();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'email' => $model->username,
            ];
            $response = (Object) $service->requestPasswordReset($data);
            if ($response->status['code'] == 100) {
                $url = Html::a("Back to login", ['/site/login'], ['class' => 'btn btn-small btn-success']);
                \Yii::$app->getSession()->setFlash('success-message', "Password reset request was successful. Check your email for reset instructions... " . $url);
                return $this->redirect(['info']);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('@common/views/requestPasswordReset', [
                    'model' => $model,
        ]);
    }

    public function actionChangePassword() {
        $model = new ChangePassword();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->changePassword($model);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Password change was successful");
                return $this->redirect(['info']);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('@common/views/changepassword', [
                    'model' => $model,
        ]);
    }

    public function actionResetPassword() {
        $model = new ResetPassword();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $data = [
                'id' => Yii::$app->session->get('credentialId'),
                'password' => $model->password,
            ];
            $response = (Object) $service->resetPassword($data);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Password reset was successful.");
                return $this->redirect('login');
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('@common/views/resetPassword', [
                    'model' => $model,
        ]);
    }

    public function actionInfo() {
        return $this->render('@common/views/message');
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
