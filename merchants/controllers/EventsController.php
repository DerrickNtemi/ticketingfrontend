<?php

namespace merchants\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Service;
use common\models\Events;
use yii\web\Response;
use yii\web\UploadedFile;
use common\models\EventTickets;
use common\models\EventSponsor;
use common\models\EventGallery;
use yii\helpers\Url;

/**
 * Site controller
 */
class EventsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $data = $service->getMerchantEvents(1);
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('index', ['data' => $events]);
    }

    public function actionPending() {
        $service = new Service();
        $data = $service->getMerchantEvents(7);
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('pending', ['data' => $events]);
    }

    public function actionHistory() {
        $service = new Service();
        $data = $service->getMerchantEvents();
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('history', ['data' => $events]);
    }

    public function actionCancel($id) {
        $service = new Service();
        $response = (Object) $service->deleteEvent($id);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Event sucessfully cancelled.");
        } else {
            \Yii::$app->getSession()->setFlash('fail-message', $response->status['message']);
        }
        return $this->redirect(['index']);
    }

    public function actionAuthorize($id) {
        $service = new Service();
        $response = (Object) $service->authorizeEvent($id);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Event sucessfully authorized.");
        } else {
            \Yii::$app->getSession()->setFlash('fail-message', $response->status['message']);
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    public function actionCreate() {
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $model = new Events();
        $pendingEvent = $service->getPendingEvent();
        if (!empty($pendingEvent)) {
            foreach ($pendingEvent as $event) {
                $model->setAttributes($event);
            }
        }
        $eventSponsorModel = new EventSponsor();
        $eventGalleryModel = new EventGallery();
        $ticketModel = new EventTickets();
        $eventTypesData = $service->getEventTypes();
        $countryData = $service->getCountry();
        $countyData = $service->getCounty();
        $tickettypeData = $service->getTicketTypes();
        $ticketnameData = $service->getTicketNames(1);
        $ticketNames = $ticketTypes = $counties = $countries = $eventTypes = $eventTickets = $eventSponsors = $eventGalleries = [];
        if (!empty($model->id)) {
            $eventTicketsData = $service->getEventTicketTiers($model->id, 1);
            foreach ($eventTicketsData as $eventTicket) {
                $ticketsModel = new EventTickets();
                $ticketsModel->setAttributes($eventTicket);
                $eventTickets[] = $ticketsModel;
            }
            $eventGalleryData = $service->getEventGalleries($model->id, $identity->merchantId['id'], 1);
            foreach ($eventGalleryData as $data) {
                $dataModel = new EventGallery();
                $dataModel->setAttributes($data);
                $eventGalleries[] = $dataModel;
            }
            $eventSponsorData = $service->getEventSponsors($model->id, $identity->merchantId['id'], 1);
            foreach ($eventSponsorData as $data) {
                $dataModel = new EventSponsor();
                $dataModel->setAttributes($data);
                $eventSponsors[] = $dataModel;
            }
        }
        foreach ($eventTypesData as $data) {
            $eventTypes[$data['id']] = $data['name'];
        }
        foreach ($countyData as $data) {
            $counties[$data['id']] = $data['name'];
        }
        foreach ($countryData as $data) {
            $countries[$data['id']] = $data['name'];
        }
        $ticketNames[] = "Select Ticket Name";
        foreach ($ticketnameData as $data) {
            $ticketNames[$data['id']] = $data['name'];
        }
        $ticketTypes[] = "Select Ticket Type";
        foreach ($tickettypeData as $data) {
            $ticketTypes[$data['id']] = $data['name'];
        }
        return $this->render('create', [
                    'model' => $model,
                    'eventTypes' => $eventTypes,
                    'countries' => $countries,
                    'counties' => $counties,
                    'ticketModel' => $ticketModel,
                    'ticketNames' => $ticketNames,
                    'ticketTypes' => $ticketTypes,
                    'eventTickets' => $eventTickets,
                    'eventSponsorModel' => $eventSponsorModel,
                    'eventGalleryModel' => $eventGalleryModel,
                    'eventGalleries' => $eventGalleries,
                    'eventSponsors' => $eventSponsors
        ]);
    }

    public function actionUpdate($id) {
        $model = new Events();
        $eventSponsorModel = new EventSponsor();
        $eventGalleryModel = new EventGallery();
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $eventDetails = $service->getEventDetails($id);
        $eventTickets = $eventGalleries = $eventSponsors = [];
        $model->setAttributes($eventDetails['event']);
        foreach ($eventDetails['tickets'] as $eventTicket) {
            $ticketsModel = new EventTickets();
            $ticketsModel->setAttributes($eventTicket);
            $eventTickets[] = $ticketsModel;
        }

        $eventGalleryData = $service->getEventGalleries($model->id, $identity->merchantId['id'], 1);
        foreach ($eventGalleryData as $data) {
            $dataModel = new EventGallery();
            $dataModel->setAttributes($data);
            $eventGalleries[] = $dataModel;
        }
        $eventSponsorData = $service->getEventSponsors($model->id, $identity->merchantId['id'], 1);
        foreach ($eventSponsorData as $data) {
            $dataModel = new EventSponsor();
            $dataModel->setAttributes($data);
            $eventSponsors[] = $dataModel;
        }

        $ticketModel = new EventTickets();
        $eventTypesData = $service->getEventTypes();
        $countryData = $service->getCountry();
        $countyData = $service->getCounty();
        $tickettypeData = $service->getTicketTypes();
        $ticketnameData = $service->getTicketNames(1);
        $ticketNames = $ticketTypes = $counties = $countries = $eventTypes = [];

        foreach ($eventTypesData as $data) {
            $eventTypes[$data['id']] = $data['name'];
        }
        foreach ($countyData as $data) {
            $counties[$data['id']] = $data['name'];
        }
        foreach ($countryData as $data) {
            $countries[$data['id']] = $data['name'];
        }
        $ticketNames[] = "Select Ticket Name";
        foreach ($ticketnameData as $data) {
            $ticketNames[$data['id']] = $data['name'];
        }
        $ticketTypes[] = "Select Ticket Type";
        foreach ($tickettypeData as $data) {
            $ticketTypes[$data['id']] = $data['name'];
        }
        return $this->render('update', [
                    'model' => $model,
                    'eventTypes' => $eventTypes,
                    'countries' => $countries,
                    'counties' => $counties,
                    'ticketModel' => $ticketModel,
                    'ticketNames' => $ticketNames,
                    'ticketTypes' => $ticketTypes,
                    'eventTickets' => $eventTickets,
                    'eventSponsorModel' => $eventSponsorModel,
                    'eventGalleryModel' => $eventGalleryModel,
                    'eventGalleries' => $eventGalleries,
                    'eventSponsors' => $eventSponsors
        ]);
    }

    public function actionDetails() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new Events();
        $model->load(Yii::$app->request->post());

        $poster = UploadedFile::getInstance($model, 'poster');
        if ($poster) {
            $name = $poster->name;
            $path = Yii::getAlias('@webroot') . '/uploads/posters/' . $name;
            $poster->saveAs($path);
            $model->poster = $name;
        } else {
            if (!empty($model->id)) {
                $modelData = new Events();
                $modelData->setAttributes($service->getSingleEvent($model->id));
                $model->poster = $modelData->poster;
            }
        }
        if ($model->validate()) {
            $response = (Object) $service->createEventDetails($model);
            if ($response->status['code'] == 100) {
                return ['success' => $response->item];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = "";
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDetailsUpdate() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new Events();
        $model->load(Yii::$app->request->post());

        $poster = UploadedFile::getInstance($model, 'poster');
        if ($poster) {
            $name = $poster->name;
            $path = Yii::getAlias('@webroot') . '/uploads/posters/' . $name;
            $poster->saveAs($path);
            $model->poster = $name;
        } else {
            if (!empty($model->id)) {
                $modelData = new Events();
                $modelData->setAttributes($service->getSingleEvent($model->id));
                $model->poster = $modelData->poster;
            }
        }
        if ($model->validate()) {
            $response = (Object) $service->createEventDetails($model);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Event details successfully updated.");
                $url = Url::toRoute(['update', 'id' => $response->item]);
                return ['success' => ['url' => $url]];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = "";
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateTicketTier() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new EventTickets();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $response = (Object) $service->createTicketTier($model);
            if ($response->status['code'] == 100) {
                $eventTicketTiersData = $service->getEventTicketTiers($model->eventsId, 1);
                $ticketTiers = [];
                foreach ($eventTicketTiersData as $data) {
                    $eventTickets = new EventTickets();
                    $eventTickets->setAttributes($data);
                    $ticketTiers[] = $eventTickets;
                }
                $message = empty($model->id) ? 'Ticket tier successfully created' : 'Record successfully updated';
                $response = ['results' => $this->renderPartial('_tickets', ['tickets' => $ticketTiers], false, true), 'message' => $message];
                return ['success' => $response];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = "";
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateEventSponsor() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new EventSponsor();
        $model->load(Yii::$app->request->post());
        $logo = UploadedFile::getInstance($model, 'logo');

        $identity = Yii::$app->user->getIdentity();
        if ($logo) {
            $name = $logo->name;
            $path = Yii::getAlias('@webroot') . '/uploads/sponsors/logo/' . $name;
            $logo->saveAs($path);
            $model->logo = $name;
        } else {
            if (!empty($model->id)) {
                $modelData = new EventSponsor();
                $modelData->setAttributes($service->getEventSponsor($model->id, $identity->merchantId['id']));
                $model->logo = $modelData->logo;
            }
        }

        if ($model->validate()) {
            $model->merchantId = $identity->merchantId['id'];
            $response = (Object) $service->createEventSponsor($model);
            if ($response->status['code'] == 100) {
                $eventSponsors = [];
                $eventSponsorData = $service->getEventSponsors($model->eventId, $identity->merchantId['id'], 1);
                foreach ($eventSponsorData as $data) {
                    $dataModel = new EventSponsor();
                    $dataModel->setAttributes($data);
                    $eventSponsors[] = $dataModel;
                }
                $message = empty($model->id) ? 'Event sponsor successfully created' : 'Record successfully updated';
                $response = ['results' => $this->renderPartial('_sponsors', ['sponsors' => $eventSponsors], false, true), 'message' => $message];
                return ['success' => $response];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = "";
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionCreateEventGallery() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $service = new Service();
        $model = new EventGallery();
        $identity = Yii::$app->user->getIdentity();
        $model->load(Yii::$app->request->post());
        $photo = UploadedFile::getInstance($model, 'photo');
        if ($photo) {
            $name = $photo->name;
            $path = Yii::getAlias('@webroot') . '/uploads/gallery/' . $name;
            $photo->saveAs($path);
            $model->photo = $name;
        } else {
            if (!empty($model->id)) {
                $modelData = new EventGallery();
                $modelData->setAttributes($service->getEventGallery($model->id, $identity->merchantId['id']));
                $model->photo = $modelData->photo;
            }
        }

        if ($model->validate()) {
            $identity = Yii::$app->user->getIdentity();
            $model->merchantId = $identity->merchantId['id'];
            $response = (Object) $service->createEventGallery($model);
            if ($response->status['code'] == 100) {
                $eventGalleries = [];
                $eventGalleryData = $service->getEventGalleries($model->eventId, $identity->merchantId['id'], 1);
                foreach ($eventGalleryData as $data) {
                    $dataModel = new EventGallery();
                    $dataModel->setAttributes($data);
                    $eventGalleries[] = $dataModel;
                }
                $message = empty($model->id) ? 'Event gallery successfully created' : 'Record successfully updated';
                $response = ['results' => $this->renderPartial('_galleries', ['galleries' => $eventGalleries], false, true), 'message' => $message];
                return ['success' => $response];
            } else {
                $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
            }
        }
        $errorsArry = "";
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteTicketTier($id, $eventsId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new EventTickets();
        $service = new Service();
        $response = (Object) $service->deleteTicketTier($id);
        if ($response->status['code'] == 100) {
            $eventTicketTiersData = $service->getEventTicketTiers($eventsId, 1);
            $ticketTiers = [];
            foreach ($eventTicketTiersData as $data) {
                $eventTickets = new EventTickets();
                $eventTickets->setAttributes($data);
                $ticketTiers[] = $eventTickets;
            }
            $message = 'Ticket tier successfully delete';
            $response = ['results' => $this->renderPartial('_tickets', ['tickets' => $ticketTiers], false, true), 'message' => $message];
            return ['success' => $response];
        } else {
            $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteEventSponsor($id, $eventId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new EventSponsor();
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $response = (Object) $service->deleteEventSponsors($id, $eventId, $identity->merchantId['id']);
        if ($response->status['code'] == 100) {
            $eventSponsors = [];
            $eventSponsorData = $service->getEventSponsors($eventId, $identity->merchantId['id'], 1);
            foreach ($eventSponsorData as $data) {
                $dataModel = new EventSponsor();
                $dataModel->setAttributes($data);
                $eventSponsors[] = $dataModel;
            }
            $message = 'Record successfully deleted';
            $response = ['results' => $this->renderPartial('_sponsors', ['sponsors' => $eventSponsors], false, true), 'message' => $message];
            return ['success' => $response];
        } else {
            $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDeleteEventGallery($id, $eventId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new EventGallery();
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $response = (Object) $service->deleteEventGallery($id, $eventId, $identity->merchantId['id']);
        if ($response->status['code'] == 100) {
            $eventGalleries = [];
            $eventGalleryData = $service->getEventGalleries($eventId, $identity->merchantId['id'], 1);
            foreach ($eventGalleryData as $data) {
                $dataModel = new EventGallery();
                $dataModel->setAttributes($data);
                $eventGalleries[] = $dataModel;
            }
            $message = 'Record successfully deleted';
            $response = ['results' => $this->renderPartial('_galleries', ['galleries' => $eventGalleries], false, true), 'message' => $message];
            return ['success' => $response];
        } else {
            $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionChangeTicketUnits($id, $eventsId, $units) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new EventTickets();
        $service = new Service();
        $response = (Object) $service->changeTicketUnits($id, $units);
        if ($response->status['code'] == 100) {
            $eventTicketTiersData = $service->getEventTicketTiers($eventsId, 1);
            $ticketTiers = [];
            foreach ($eventTicketTiersData as $data) {
                $eventTickets = new EventTickets();
                $eventTickets->setAttributes($data);
                $ticketTiers[] = $eventTickets;
            }
            $message = 'Ticket tier units successfully updated';
            $response = ['results' => $this->renderPartial('_tickets', ['tickets' => $ticketTiers], false, true), 'message' => $message];
            return ['success' => $response];
        } else {
            $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionDetailsChecktickets($eventsId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Events();
        $service = new Service();

        $eventTicketTiersData = $service->getEventTicketTiers($eventsId, 1);
        if (empty($eventTicketTiersData)) {
            $model->addError("", "You must enter atleast one ticket tier");
            $errorsArry = '';
            foreach ($model->getErrors() as $error) {
                $errorsArry .= $error[0] . "<br/>";
            }
            return ['error' => $errorsArry];
        }
        return ['success' => 'success'];
    }

    public function actionDetailsFinishDisplay($eventsId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Events();
        $service = new Service();
        $eventDetails = $service->getEventDetails($eventsId);
        $eventTickets = $eventGalleries = $eventSponsors = [];
        $model->setAttributes($eventDetails['event']);
        //print_r($eventDetails); exit;
        foreach ($eventDetails['tickets'] as $eventTicket) {
            $ticketsModel = new EventTickets();
            $ticketsModel->setAttributes($eventTicket);
            $eventTickets[] = $ticketsModel;
        }

        foreach ($eventDetails['gallery'] as $data) {
            $dataModel = new EventGallery();
            $dataModel->setAttributes($data);
            $eventGalleries[] = $dataModel;
        }

        foreach ($eventDetails['sponsors'] as $data) {
            $dataModel = new EventSponsor();
            $dataModel->setAttributes($data);
            $eventSponsors[] = $dataModel;
        }
        return ['success' => $this->renderPartial("_finish", ['model' => $model, 'tickettiers' => $eventTickets, 'sponsors' => $eventSponsors, 'gallery' => $eventGalleries, 'visibility' => false], false, true)];
    }

    public function actionDetailsFinish($eventsId) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new EventTickets();
        $service = new Service();
        $response = (Object) $service->completeEventCreation($eventsId);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Successful creation of event. Kindly awaiting event publication.");
            $this->redirect(['view', 'id' => $eventsId]);
        } else {
            $model->addError("", "Error Code " . $response->status['code'] . " :: " . $response->status['message']);
        }
        $errorsArry = '';
        foreach ($model->getErrors() as $error) {
            $errorsArry .= $error[0] . "<br/>";
        }
        return ['error' => $errorsArry];
    }

    public function actionView($id) {
        $model = new Events();
        $service = new Service();
        $eventDetails = $service->getEventDetails($id);
        $eventTickets = $eventGalleries = $eventSponsors = [];
        $model->setAttributes($eventDetails['event']);
        foreach ($eventDetails['tickets'] as $eventTicket) {
            $ticketsModel = new EventTickets();
            $ticketsModel->setAttributes($eventTicket);
            $eventTickets[] = $ticketsModel;
        }

        foreach ($eventDetails['gallery'] as $data) {
            $dataModel = new EventGallery();
            $dataModel->setAttributes($data);
            $eventGalleries[] = $dataModel;
        }

        foreach ($eventDetails['sponsors'] as $data) {
            $dataModel = new EventSponsor();
            $dataModel->setAttributes($data);
            $eventSponsors[] = $dataModel;
        }
        return $this->render('view', [
                    'model' => $model,
                    'tickettiers' => $eventTickets,
                    'sponsors' => $eventSponsors,
                    'gallery' => $eventGalleries,
                    'visibility' => false,
        ]);
    }

}
