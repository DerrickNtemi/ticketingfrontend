<?php

namespace merchants\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Service;
use common\models\TicketNames;

/**
 * Site controller
 */
class SettingsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionTicketNames() {
        $service = new Service();
        $data = $service->getTicketNames(1);
        $ticketNames = [];
        foreach ($data as $dt) {
            $model = new TicketNames();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $ticketNames[] = $model;
        }
        return $this->render('ticketnames', ['data' => $ticketNames]);
    }

    public function actionNewTicketName() {
        $model = new TicketNames();
        $service = new Service();
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->createTicketName($model);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Ticket Name sucessfully created.");
                return $this->redirect(['view-ticket-name', 'id' => $response->item]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('createticketname', ['model' => $model]);
    }

    public function actionViewTicketName($id) {
        $model = new TicketNames();
        $service = new Service();
        $model->setAttributes($service->getTicketName($id));
        $model->createdAt = $service->timestampToString1($model->createdAt);
        return $this->render('viewticketname', ['model' => $model]);
    }

    public function actionDeleteTicketName($id) {
        $service = new Service();
        $response = (Object) $service->ticketNameDeletion($id);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "Ticket name sucessfully deleted.");
        } else {
            \Yii::$app->getSession()->setFlash('fail-message', $response->status['message']);
        }
        return $this->redirect(['ticket-names']);
    }

}
