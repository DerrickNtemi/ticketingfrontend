<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;

$this->title = 'Account Creation';
$this->params['breadc rumbs'][] = $this->title;
?>
<div class="site-login" style="margin-bottom: 50px;">
    <div class="row" >
        <div id="login-container" style="float: none; margin: 0 auto; min-width: 800px;">
            <h1 align="left" id="form-title" class="title-colors">CREATE <?= $appName . " ACCOUNT " ?></h1>
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'account-creation-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <div class="panel panel-default">
                <div class="panel-heading"  id="title-colors">Business Information</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8">
                            <?= $form->field($model, 'businessName', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-th-list"></span>']], 'inputOptions' => ['placeholder' => 'Business Name']])->textInput(['autofocus' => true])->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'email', ['addon' => ['prepend' => ['content' => '@']], 'inputOptions' => ['placeholder' => 'Email']])->textInput(['autofocus' => true])->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'town', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-map-marker"></span>']], 'inputOptions' => ['placeholder' => 'Town/city']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'businessAddress', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-envelope"></span>']], 'inputOptions' => ['placeholder' => 'Business Address']])->textInput(['autofocus' => true])->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"><?=
                            $form->field($model, 'countryId')->widget(Select2::classname(), [
                                'data' => $countries,
                                'options' => ['placeholder' => 'Select Country ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'email', ['addon' => ['prepend' => ['content' => '@']], 'inputOptions' => ['placeholder' => 'Email']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'phoneNumber', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-phone"></span>']], 'inputOptions' => ['placeholder' => 'Phone Number']])->textInput()->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <?= $form->field($model, 'businessUrl', ['addon' => ['prepend' => ['content' => '<span>http://example.com</span>']], 'inputOptions' => ['placeholder' => 'Business Url']])->textInput()->label() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" id="title-colors">Contact Person</div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonFirstName', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']],'inputOptions' => ['placeholder' => 'First Name']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonMiddleName', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']],'inputOptions' => ['placeholder' => 'Middle Name']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonLastName', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']],'inputOptions' => ['placeholder' => 'Last Name']])->textInput()->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonEmail', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-phone"></span>']], 'inputOptions' => ['placeholder' => 'Email']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonPhoneNumber', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-phone"></span>']], 'inputOptions' => ['placeholder' => 'Phone Number']])->textInput()->label() ?>
                        </div>
                        <div class="col-md-4">
                            <?= $form->field($model, 'contactPersonPosition', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-phone"></span>']], 'inputOptions' => ['placeholder' => 'Position']])->textInput()->label() ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Create Account', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <div id="login-help">
                <?= Html::a("Have account? click to login", ['site/login']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
