<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
    <div class="row signup-main-row">
        <div class="col-lg-1"></div>
        <div class="col-lg-4">
            <?php
                $form = ActiveForm::begin(['id' => 'request-password-reset-form']);

                if ($model->hasErrors()) {
                    echo $form->errorSummary($model, ['header' => '']);
                }
            ?>

            <?= $form->field($model, 'username', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']], 'inputOptions' => ['placeholder' => 'username']])->textInput(['autofocus' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Request Reset', ['class' => 'btn btn-primary', 'style' => 'border-radius:0px;',]) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
