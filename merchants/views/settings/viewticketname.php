<?php

use yii\helpers\Url;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Ticket Name');
$this->params['breadcrumbs'][] = $this->title;
                    $url = Url::toRoute(['delete-ticket-name', 'id' => $model->id]);
?>
<div class="row members-index">
    <div class="col-md-12">
        <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
            <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('success-message')): ?>
            <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
        <?php endif; ?>
        <div class="row" >
            <div class="col-md-12 text-right">
                <p>
                    <?= Html::a('<span class="glyphicon glyphicon-list"></span> List', ['ticket-names'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-info', 'title' => Yii::t('yii', 'list ticket names'),]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-plus"></span> Add', ['new-ticket-name'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-primary', 'title' => Yii::t('yii', 'Add ticket name'),]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-remove"></span> Delete', $url, ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-danger', 'title' => Yii::t('yii', 'delete ticket names'),]); ?>
                </p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title . " : " . $model->name ?></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <table class="table table-striped">
                            <tr>
                                <th>Name</th>
                                <th>:</th>
                                <td><?= $model->name ?></td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <th>:</th>
                                <td><?= $model->statusId['name'] ?></td>
                            </tr>
                            <tr>
                                <th>Timestamp</th>
                                <th>:</th>
                                <td><?= $model->createdAt ?></td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <th>:</th>
                                <td><?= $model->createdBy['email'] ?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

