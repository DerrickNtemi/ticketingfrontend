<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Approve Merchant Account';
$this->params['breadcrumbs'][] = "Approve [".$model->name."] Merchant Account";
?>
<div class="site-login" style="margin-bottom: 50px;">


    <div class="row" >
        <div id="login-container" style="float: none; ">
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'verify-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'remarks', ['inputOptions' => ['placeholder' => 'remarks']])->textarea(['autofocus' => true])->label() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'walletKeyOne', ['inputOptions' => ['placeholder' => 'consumer key']])->textInput()->label() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'walletKeyTwo', ['inputOptions' => ['placeholder' => 'consumer secret key']])->textInput()->label() ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Verify', ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'width:100%;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
