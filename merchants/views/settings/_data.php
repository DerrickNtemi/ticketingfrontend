<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider([ 'allModels' => $data]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label'=>'Name',
            'value'=>function($data){
                return $data->name;
            }
        ],
        [
            'label'=>'Timestamp',
            'value'=>function($data){
                return $data->createdAt;
            }
        ],
        [
            'label'=>'Status',
            'value'=>function($data){
                return $data->statusId['name'];
            }
        ],
        [
            'label'=>'Created By',
            'value'=>function($data){
                return $data->createdBy['email'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                'view' => function($url, $model) {
                    $url = Url::toRoute(['settings/view-ticket-name', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['settings/delete-ticket-name', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Cancel Event'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>