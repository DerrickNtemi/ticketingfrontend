<?php

use yii\helpers\Html;

$this->title = Yii::t('app', 'Ticket Name List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
            <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('success-message')): ?>
            <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
        <?php endif; ?>
        <div class="row" >
            <div class="col-md-12 text-right">
                <p><?= Html::a('<span class="glyphicon glyphicon-plus"></span> Add Ticket Name', ['new-ticket-name'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-primary', 'title' => Yii::t('yii', 'Add ticket name'),]); ?></p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <?= $this->render('_data', ['data' => $data]) ?>
            </div>
        </div>
    </div>
</div>

