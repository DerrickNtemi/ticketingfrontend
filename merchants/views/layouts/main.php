<?php

/* @var $this \yii\web\View */
/* @var $content string */

use merchants\assets\MerchantsAsset;
use kartik\sidenav\SideNav;

MerchantsAsset::register($this);
$this->beginBlock('appName');
echo "Merchant Portal";
$this->endBlock();
?>
<?php $this->beginBlock('menu') ?>
<?php if (!Yii::$app->user->isGuest): ?>
    <?=

    SideNav::widget([
        'type' => SideNav::TYPE_INFO,
        'heading' => '',
        'items' => [
            [
                'label' => 'Dashboard',
                'icon' => 'dashboard',
                'url' => ['site/index'],
            ],
            [
                'label' => 'Events',
                'icon' => 'list',
                'items' => [
                    ['label' => 'Create Event', 'icon' => '', 'url' => ['events/create']],
                    ['label' => 'Events Awaiting Publication', 'icon' => '', 'url' => ['events/pending']],
                    ['label' => 'Active Events', 'icon' => '', 'url' => ['events/index']],
                    ['label' => 'Events History', 'icon' => '', 'url' => ['events/history']],
                ],
            ],
            [
                'label' => 'Settings',
                'icon' => 'cog',
                'url' => '#',
                'items' => [
                    ['label' => 'Ticket Names', 'icon' => '', 'url' => ['settings/ticket-names']],
                ],
            ],
            [
                'label' => 'Users',
                'icon' => 'user',
                'url' => '#',
                'items' => [
                    ['label' => 'Users List', 'icon' => '', 'url' => ['users/index']],
                    ['label' => 'Create', 'icon' => '', 'url' => ['users/create']],
                ],
            ],
            [
                'label' => 'Reports',
                'icon' => 'folder-open',
                'url' => '#',
                'items' => [
                    ['label' => 'Events List', 'icon' => '', 'url' => '#'],
                    ['label' => 'Revenue', 'icon' => '', 'url' => '#'],
                ],
            ],/*
            [
                'label' => 'Recycle Bin',
                'icon' => 'th',
                'url' => '#',
                'items' => [
                    ['label' => 'Users', 'icon' => '', 'url' => ['users/recycle']]
                ],
            ],*/
        ],
    ]);
    ?>
<?php endif; ?>
<?php $this->endBlock() ?>
<?= $this->render('@common/views/layouts/main', ['content' => $content]) ?>