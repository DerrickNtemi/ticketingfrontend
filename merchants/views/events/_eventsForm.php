<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use kartik\widgets\Select2;
?>
<?php
$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['events/details']),
            'id' => 'events-details-form',
            'enableClientValidation' => true,
        ]);
?>
<?= Html::activeHiddenInput($model, 'id') ?>
<div class="row">
    <div class="col-md-12">        
        <div class="row">
            <div class="col-md-4">
                <?=
                $form->field($model, 'name', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-th"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textInput()
                ?>
            </div>
            <div class="col-md-4"><?=
                $form->field($model, 'eventTypeId')->widget(Select2::classname(), [
                    'data' => $eventTypes,
                    'options' => ['placeholder' => 'Select Event Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-4">                
                <?=
                $form->field($model, 'poster', [
                    'options' => ['accept' => 'image/*'],
                ])->fileInput();
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">    
                <?=
                $form->field($model, 'location', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-globe"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textInput()
                ?>
            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'venue', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-globe"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textInput()
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">    
                <?=
                $form->field($model, 'latitude', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-map-marker"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textInput()
                ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($model, 'longitude', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-map-marker"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textInput()
                ?>
            </div><div class="col-md-3">   <?=
                $form->field($model, 'countryId')->widget(Select2::classname(), [
                    'data' => $countries,
                    'options' => ['placeholder' => 'Select country ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-3"><?=
                $form->field($model, 'countyId')->widget(Select2::classname(), [
                    'data' => $counties,
                    'options' => ['placeholder' => 'Select county ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">                
                <?=
                $form->field($model, 'startDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])
                ?>
            </div>
            <div class="col-md-6">            
                <?=
                $form->field($model, 'startTime', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(TimePicker::classname(), [
                    'value' => date("hh:mm"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'hh:mm',
                        'showMeridian' => false,
                        'minuteStep' => 5,
                    ]
                ])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">                
                <?=
                $form->field($model, 'endDate', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DatePicker::classname(), [
                    'value' => date("Y-m-d"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true,
                    ]
                ])
                ?>
            </div>
            <div class="col-md-6">            
                <?=
                $form->field($model, 'endTime', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(TimePicker::classname(), [
                    'value' => date("hh:mm"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'hh:mm',
                        'showMeridian' => false,
                        'minuteStep' => 5,
                    ]
                ])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'description', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-list"></span>']], 'inputOptions' => ['class' => 'form-control  form-control-custom'],])->textarea(['rows' => 7]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
