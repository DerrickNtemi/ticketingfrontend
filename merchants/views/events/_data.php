<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider([ 'allModels' => $data]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label'=>'Event Name',
            'value'=>function($data){
                return $data->name;
            }
        ],
        [
            'label'=>'Start Date',
            'value'=>function($data){
                return $data->startDate;
            }
        ],
        [
            'label'=>'Start Time',
            'value'=>function($data){
                return $data->startTime;
            }
        ],
        [
            'label'=>'End Date',
            'value'=>function($data){
                return $data->endDate;
            }
        ],
        [
            'label'=>'End Time',
            'value'=>function($data){
                return $data->endTime;
            }
        ],
        [
            'label'=>'Status',
            'value'=>function($data){
                return $data->statusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{delete}&nbsp;&nbsp;{authorize}&nbsp;&nbsp;',
            'buttons' => [
                'view' => function($url, $model) {
                    $url = Url::toRoute(['events/view', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['events/cancel', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Cancel'),]);
                },
                'authorize' => function($url, $model) {
                    $url = Url::toRoute(['events/authorize', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Authorize'), 'class'=>$model->statusId['id'] == 7 ? "disable-merchant-verification" : "enable-merchant-verification"]);
                },
                    ],
                ],
            ],
        ]);
        ?>