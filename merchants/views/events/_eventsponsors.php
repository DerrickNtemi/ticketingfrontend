<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
?>
<?php
$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['events/create-event-sponsor']),
            'id' => 'event-sponsor-form',
            'enableClientValidation' => true,
        ]);
?>                
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'eventId') ?>
<?= Html::activeHiddenInput($model, 'merchantId') ?>
<div class="row">
    <div class="col-md-12">     
        <div class="row">
            <div class="col-md-6">
                <?=
                $form->field($model, 'name', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-bookmark"></span>']], 
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'itemSponsoring', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-tasks"></span>']], 
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7">
                <?=
                $form->field($model, 'businessUrl', ['addon' => ['prepend' => ['content' => '<span>www.example.com</span>']], 
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
            <div class="col-md-5">              
                <?=
                $form->field($model, 'logo', [
                    'options' => ['accept' => 'image/*'],
                ])->fileInput();
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
