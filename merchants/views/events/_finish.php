<?php

use yii\helpers\Html;
?>
<div class="form-group">
    <div class="alert alert-danger"  id='finish-error-summary' role="alert" style="display: none;"> </div>
</div>
<div class="form-group mt10">
    <div class="panel panel-default">
        <div class="panel-heading">
            Event Details
        </div>
        <div class="panel-body">
            <div class="row heading-row">
                <div class="col-md-3">
                    <label><b>Name : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Event Type: </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Country: </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>County: </b></label>
                </div>
            </div>     
            <div class="row details-row">
                <div class="col-md-3">
                    <?php echo Html::encode($model->name); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->eventTypeId['name']); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->countryId['name']); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->countyId['name']); ?>
                </div>
            </div>             
            <div class="row heading-row">
                <div class="col-md-3">
                    <label><b>Venue : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Location : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Latitude : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Longitude : </b></label>
                </div>
            </div>   
            <div class="row details-row">
                <div class="col-md-3">
                    <?php echo Html::encode($model->venue); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->location); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->latitude); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->longitude); ?>
                </div>
            </div>              
            <div class="row heading-row">
                <div class="col-md-3">
                    <label><b>Start Date : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>Start Time : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>End Date : </b></label>
                </div>
                <div class="col-md-3">
                    <label><b>End Time : </b></label>
                </div>
            </div>   
            <div class="row details-row">
                <div class="col-md-3">
                    <?php echo Html::encode($model->startDate); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->startTime); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->endDate); ?>
                </div>
                <div class="col-md-3">
                    <?php echo Html::encode($model->endTime); ?>
                </div>
            </div>  
            <div class="row heading-row">
                <div class="col-md-6">
                    <label><b>Description : </b></label>
                </div>
                <div class="col-md-6">
                    <label><b>Poster : </b></label>
                </div>
            </div>  
            <div class="row details-row" style="padding-top: 5px;">
                <div class="col-md-6">
                    <?php echo Html::encode($model->description); ?>
                </div>
                <div class="col-md-6">
                    <?= Html::img("@web/uploads/posters/$model->poster", ['class' => 'img-responsive', 'style' => 'width:300px; height:200px;']) ?>
                </div>
            </div>    
        </div>
    </div>
    <?php if(count($tickettiers) > 0) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Ticket Tiers
        </div>
        <div class="panel-body grid-font-formating" >
            <?= $this->render('_tickets', ['tickets' => $tickettiers, 'visibility' => $visibility]) ?>
        </div>
    </div> 
    <?php endif; ?>
    <?php if(count($sponsors) > 0) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Sponsors
        </div>
        <div class="panel-body" >
            <?php
            $i = 0;
            $items = count($sponsors);
            foreach ($sponsors as $data) {
                if ($i % 3 == 0) {
                    ?><div class="row"><?php
                }
                ?>
                <div class="col-md-4 col-lg-4 main-card-holder"> 
                    <div class="row inner-image-card-row">
                        <div class="col-md-12  col-lg-12 image-card-holder">
                            <div class="row image-card-holder-row">
                                <div class="col-md-12">
                                    <?= Html::img("@web/uploads/sponsors/logo/$data->logo", ['class' => 'img-responsive']) ?>
                                </div>
                            </div>
                            <div class="row details-card-holder-row">
                                <div class="col-md-12">
                                    <h4><?= $data->name ?></h4>
                                    <p><?= $data->businessUrl ?></p>
                                    <hr/>
                                    <p><?= $data->itemSponsoring ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                if ($i % 3 == 0 || $i == $items) {
                    ?></div><?php
                }
            }
            ?>
        </div>
    </div>    
    <?php endif; ?>
    <?php if(count($gallery) > 0) : ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            Gallery
        </div>
        <div class="panel-body" >
            <?php
            $i = 0;
            $items = count($gallery);
            foreach ($gallery as $data) {
                if ($i % 3 == 0) {
                    ?><div class="row"><?php
                }
                ?>
                <div class="col-md-4 col-lg-4 main-card-holder"> 
                    <div class="row inner-image-card-row">
                        <div class="col-md-12  col-lg-12 image-card-holder">
                            <div class="row image-card-holder-row">
                                <div class="col-md-12">
                                    <?= Html::img("@web/uploads/gallery/$data->photo", ['class' => 'img-responsive']) ?>
                                </div>
                            </div>
                            <div class="row details-card-holder-row">
                                <div class="col-md-12">
                                    <h4><?= $data->title ?></h4>
                                    <hr/>
                                    <p><?= $data->description ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $i++;
                if ($i % 3 == 0 || $i == $items) {
                    ?></div><?php
                }
            }
            ?>
        </div>
    </div>   
    <?php endif; ?>
</div>