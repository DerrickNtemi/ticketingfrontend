<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
if (!isset($visibility)) {
    $visibility = true;
}
?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $sponsors]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Sponsor Name',
            'value' => function($data) {
                return $data->name;
            }
        ],
        [
            'label' => 'Item Sponsoring',
            'value' => function($data) {
                return $data->itemSponsoring;
            }
        ],
        [
            'label' => 'Sponsor Logo',
            'value' => function($data) {
                return $data->logo;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'visible' => $visibility,
            'buttons' => [
                'update' => function($url, $model) {
                    $data = Json::encode($model->getAttributes());
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['data-whatever' => $data, 'data-toggle' => "modal", 'data-target' => "#EventSponsorsModal", 'title' => Yii::t('yii', 'Update event sponsor'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['events/delete-event-sponsor', 'id' => $model->id, 'eventId' => $model->eventId['id']]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-value' => '#event-sponsor-success-message', 'data-div' => '#event-sponsors-details', 'data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteInnerReloadModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
            ],
        ],
    ],
]);
?>
