<?php

use yii\helpers\Html;
use kartik\datetime\DateTimePicker;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
?>
<?php
$form = ActiveForm::begin([
            'action' => Yii::$app->urlManager->createUrl(['events/create-ticket-tier']),
            'id' => 'ticket-tier-form',
            'enableClientValidation' => true,
        ]);
?>                
<?= Html::activeHiddenInput($model, 'id') ?>
<?= Html::activeHiddenInput($model, 'eventsId') ?>
<div class="row">
    <div class="col-md-12">     
        <div class="row">
            <div class="col-md-6"><?=
                $form->field($model, 'ticketTypeId')->widget(Select2::classname(), [
                    'data' => $tickettypes,
                    'options' => ['placeholder' => 'Select Ticket Type ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-6"><?=
                $form->field($model, 'ticketNameId')->widget(Select2::classname(), [
                    'data' => $ticketnames,
                    'options' => ['placeholder' => 'Select Ticket Name ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?=
                $form->field($model, 'purchaseStartDateTime', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DateTimePicker::classname(), [
                    'value' => date("Y-m-d hh:mm"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii',
                        'todayHighlight' => true,
                    ]
                ])
                ?>
            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'purchaseEndDateTime', [
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->widget(DateTimePicker::classname(), [
                    'value' => date("Y-m-d hh:mm"),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd hh:ii',
                        'todayHighlight' => true,
                    ]
                ])
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?=
                $form->field($model, 'price', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-credit-card"></span>']], 
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
            <div class="col-md-6">
                <?=
                $form->field($model, 'ticketQuantity', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-th"></span>']], 
                    'inputOptions' => ['class' => 'form-control  form-control-custom'],
                ])->textInput()
                ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<h2></h2>
<div class="alert alert-danger"  id="error-summary" style="display: none;"></div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'progress spinner']) ?>
</div>
<h2></h2>
