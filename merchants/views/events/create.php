<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Create Event');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <!-- BASIC WIZARD -->
                <div id="rootwizard"  class="basic-wizard" >
                    <ul class="nav nav-pills nav-justified tab-header-formatting" id="myTab">
                        <li><a href="#tab1" data-toggle="tab"><span>Step 1:</span> Details</a></li>
                        <li><a href="#tab2" data-toggle="tab"><span>Step 2:</span> Ticket Tiers</a></li>
                        <li><a href="#tab3" data-toggle="tab"><span>Step 3:</span> Sponsors</a></li>
                        <li><a href="#tab4" data-toggle="tab"><span>Step 4:</span> Gallery</a></li>
                        <li><a href="#tab5" data-toggle="tab"><span>Step 5:</span> Finish</a></li>
                    </ul>
                    <h2></h2>
                    <div id="bar" class="progress">
                        <div class="bar progress-bar progress-bar-striped active" id="wizard-progress-bar"></div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <h2></h2><h2></h2>
                            <div class="alert alert-danger"  id='events-details-error-summary' style="display: none;"> </div>
                            <div>
                                <?=
                                $this->render('_eventsForm', [
                                    'model' => $model,
                                    'eventTypes' => $eventTypes,
                                    'countries' => $countries,
                                    'counties' => $counties,
                                ])
                                ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab2">
                            <h2></h2><h2></h2>        
                            <div class="alert alert-danger"  id="ticket-details-error-summary" role="alert" style="display: none;"></div>
                            <div class="alert alert-success" id="ticket-details-success-message" style="display: none;"></div>
                            <p style="text-align: right;">
                                <?= Html::button('<span class="glyphicon glyphicon-plus" ></span> New Ticket Tier', ['class' => 'btn btn-primary btn-sm', 'data-whatever' => '', 'data-toggle' => "modal", 'data-target' => "#TicketTierModal"]) ?>
                            </p>   
                            <div id="ticket-tier-details" >
                                <?= $this->render('_tickets', ['tickets' => $eventTickets]) ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab3">
                            <h2></h2><h2></h2>        
                            <div class="alert alert-danger"  id="event-sponsor-error-summary" role="alert" style="display: none;"></div>
                            <div class="alert alert-success" id="event-sponsor-success-message" style="display: none;"></div>
                            <p style="text-align: right;">
                                <?= Html::button('<span class="glyphicon glyphicon-plus" ></span> Add Sponsor', ['class' => 'btn btn-primary btn-sm', 'data-whatever' => '', 'data-toggle' => "modal", 'data-target' => "#EventSponsorsModal"]) ?>
                            </p>   
                            <div id="event-sponsors-details" >
                                <?= $this->render('_sponsors', ['sponsors' => $eventSponsors]) ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4">
                            <h2></h2><h2></h2>        
                            <div class="alert alert-danger"  id="event-gallery-error-summary" role="alert" style="display: none;"></div>
                            <div class="alert alert-success" id="event-gallery-success-message" style="display: none;"></div>
                            <p style="text-align: right;">
                                <?= Html::button('<span class="glyphicon glyphicon-plus" ></span> Add Gallery Photo', ['class' => 'btn btn-primary btn-sm', 'data-whatever' => '', 'data-toggle' => "modal", 'data-target' => "#EventGalleryModal"]) ?>
                            </p>   
                            <div id="event-gallery-details" >
                                <?= $this->render('_galleries', ['galleries' => $eventGalleries]) ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab5"></div>
                    </div>

                    <ul class="pager wizard">
                        <li class="previous"><a href="javascript:;">Previous</a></li>
                        <li class="next"><a href="javascript:;">Next</a></li>
                        <li class="next finish" style="display:none;" id="create-event-btn"><a href="javascript:;">Finish</a></li>
                    </ul>
                </div>
                <div id="loadingDiv"></div>  
            </div>
        </div>
    </div>
</div>

<!-- Ticket Tier Modal -->
<?php
Modal::begin([
    'id' => 'TicketTierModal',
    'header' => '<h4 class="modal-title"  id="tickettier-modal-header" >Ticket Tier</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Create', ['id' => 'tickettier-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('_tickettier', ['model' => $ticketModel, 'ticketnames'=>$ticketNames, 'tickettypes'=>$ticketTypes]) ?>
<?php Modal::end() ?>

<!-- Delete Inner Reload  Modal -->
<?php
Modal::begin([
    'id' => 'DeleteInnerReloadModal',
    'header' => '<h4 class="modal-title"  id="delete-modal-header" >Delete Record</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Delete', ['id' => 'delete-inner-reload-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/deleteInnerReload') ?>
<?php Modal::end() ?>

<!-- update Ticket Tier Units  Modal -->
<?php
Modal::begin([
    'id' => 'ChangeTicketUnitsModal',
    'header' => '<h4 class="modal-title" >Update Ticket Tier Units</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Update', ['id' => 'update-ticket-tier-reload-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('@common/views/changeticketunits') ?>
<?php Modal::end() ?>

<!-- Event Sponsors Modal -->
<?php
Modal::begin([
    'id' => 'EventSponsorsModal',
    'header' => '<h4 class="modal-title"  id="event-sponsor-modal-header" >Event Sponsors</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Create', ['id' => 'create-event-sponsors-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('_eventsponsors', ['model' => $eventSponsorModel]) ?>
<?php Modal::end() ?>

<!-- Event Gallery Modal -->
<?php
Modal::begin([
    'id' => 'EventGalleryModal',
    'header' => '<h4 class="modal-title"  id="event-gallery-modal-header" >Event Gallery</h4>',
    'footer' =>
    Html::button('Close', ['class' => 'btn btn-default', 'data-dismiss' => 'modal'])
    . PHP_EOL .
    Html::button('Create', ['id' => 'create-event-gallery-btn', 'class' => 'btn btn-primary btn-modal-save']),
]);
?>
<?= $this->render('_eventgallery', ['model' => $eventGalleryModel]) ?>
<?php Modal::end() ?>