<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;

if (!isset($visibility)) {
    $visibility = true;
}
?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $galleries]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Photo Title',
            'value' => function($data) {
                return $data->title;
            }
        ],
        [
            'label' => 'Photo Name',
            'value' => function($data) {
                return $data->photo;
            }
        ],
        [
            'label' => 'Photo Description',
            'value' => function($data) {
                return $data->description;
            },
            'format' => 'html',
            'contentOptions' => [
                'style' => 'max-width:200px; overflow: auto; word-wrap: break-word;'
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'visible' => $visibility,
            'buttons' => [
                'update' => function($url, $model) {
                    $data = Json::encode($model->getAttributes());
                    return Html::a('<span class="glyphicon glyphicon-edit"></span>', $url, ['data-whatever' => $data, 'data-toggle' => "modal", 'data-target' => "#EventGalleryModal", 'title' => Yii::t('yii', 'Update event gallery'),]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['events/delete-event-gallery', 'id' => $model->id, 'eventId' => $model->eventId['id']]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-value' => '#event-gallery-success-message', 'data-div' => '#event-gallery-details', 'data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteInnerReloadModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
            ],
        ],
    ],
]);
?>
