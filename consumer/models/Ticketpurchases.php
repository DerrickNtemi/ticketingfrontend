<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticketpurchases".
 *
 * @property integer $id
 * @property integer $purchaseNumber
 * @property integer $eventsId
 * @property integer $eventTicketsId
 * @property integer $consumerId
 * @property integer $numberOfTickets
 * @property double $ticketAmount
 * @property double $totalAmount
 * @property double $amountPaid
 * @property double $balance
 * @property integer $createdBy
 * @property string $createdAt
 * @property integer $statusId
 * @property string $verifiiedAt
 * @property integer $verifiedBy
 */
class Ticketpurchases extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticketpurchases';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['purchaseNumber', 'eventsId', 'eventTicketsId', 'consumerId', 'numberOfTickets', 'ticketAmount', 'totalAmount', 'amountPaid', 'balance', 'createdAt', 'statusId', 'verifiiedAt', 'verifiedBy'], 'required'],
            [['purchaseNumber', 'eventsId', 'eventTicketsId', 'consumerId', 'numberOfTickets', 'createdBy', 'statusId', 'verifiedBy'], 'integer'],
            [['ticketAmount', 'totalAmount', 'amountPaid', 'balance'], 'number'],
            [['createdAt', 'verifiiedAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchaseNumber' => 'Purchase Number',
            'eventsId' => 'Events ID',
            'eventTicketsId' => 'Event Tickets ID',
            'consumerId' => 'Consumer ID',
            'numberOfTickets' => 'Number Of Tickets',
            'ticketAmount' => 'Ticket Amount',
            'totalAmount' => 'Total Amount',
            'amountPaid' => 'Amount Paid',
            'balance' => 'Balance',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'statusId' => 'Status ID',
            'verifiiedAt' => 'Verifiied At',
            'verifiedBy' => 'Verified By',
        ];
    }
}
