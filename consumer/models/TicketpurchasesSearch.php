<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ticketpurchases;

/**
 * TicketpurchasesSearch represents the model behind the search form about `app\models\Ticketpurchases`.
 */
class TicketpurchasesSearch extends Ticketpurchases
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'purchaseNumber', 'eventsId', 'eventTicketsId', 'consumerId', 'numberOfTickets', 'createdBy', 'statusId', 'verifiedBy'], 'integer'],
            [['ticketAmount', 'totalAmount', 'amountPaid', 'balance'], 'number'],
            [['createdAt', 'verifiiedAt'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ticketpurchases::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'purchaseNumber' => $this->purchaseNumber,
            'eventsId' => $this->eventsId,
            'eventTicketsId' => $this->eventTicketsId,
            'consumerId' => $this->consumerId,
            'numberOfTickets' => $this->numberOfTickets,
            'ticketAmount' => $this->ticketAmount,
            'totalAmount' => $this->totalAmount,
            'amountPaid' => $this->amountPaid,
            'balance' => $this->balance,
            'createdBy' => $this->createdBy,
            'createdAt' => $this->createdAt,
            'statusId' => $this->statusId,
            'verifiiedAt' => $this->verifiiedAt,
            'verifiedBy' => $this->verifiedBy,
        ]);

        return $dataProvider;
    }
}
