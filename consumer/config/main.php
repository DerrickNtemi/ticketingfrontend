<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
$baseUrl = '/ticketingapp/consumer/web';
return [
    'id' => 'app-consumer',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'consumer\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
		'baseUrl'=>$baseUrl,
            'csrfParam' => '_csrf-consumer',
            
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-consumer', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the consumer
            'name' => 'advanced-consumer',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'baseUrl'=> $baseUrl,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'params' => $params,
];
