<?php
/* @var $this yii\web\View */

use consumer\assets\CartAsset;
use consumer\assets\ConsumerAsset;
use yii\helpers\Html;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use richardfan\widget\JSRegister;
use yii\web\Session;

ConsumerAsset::register($this);
CartAsset::register($this);

function getColClass($items) {
    $colClass = '';
    $checker = count($items) % 3;
    switch ($checker) {
        case 1:
            $colClass = 'col-md-offset-4';
            break;
        case 2:
            $colClass = 'col-md-offset-2';
            break;
    }

    return $colClass;
}

$this->title = 'cart';
?>

<section id="home">
    <div class="wow fadeInUp">

        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                    <li><img src="../images/slider/bg1.jpg" alt="bg1"  id="wows1_0"/>
                    </li>
                    <li><img src="../images/slider/bg2.jpg" alt="slider"  id="wows1_1"/>

                    </li>
                    <li><img src="../images/slider/bg3.jpg" alt="bg3" id="wows1_2"/>

                    </li>
                </ul>
            </div>

        </div>	

    </div>
</section>

<?php //echo json_encode($details);
?> 
<section id="section-intro" class="section-wrapper about-event">
    <div class="container wow fadeInUp">
        <div class="row">
            <?php if ($detail['event']['description'] != null): ?>
                <div class=" col-xs-6  col-sm-6 col-md-6 offset-3 col-lg-6 evnt-des">
                    <h3 id="ent-description">Event Description</h3>
                    <p id= "description-p" class="lead">
                        <?= $detail['event']['description'] ?>
                    </p>
                    </br>
                    <p id="event-sp-date"> <strong>Date:</strong> <br> <?= $detail['event']['startDate'] ?> <strong>
                            to </strong> <?= $detail['event']['endDate'] ?>
                        <span id="calendar">
                            <a href=""class="cal-link">
                                <i class="fa fa-calendar"></i>
                                <span class="cal"> Add to calendar</span>
                            </a> 
                        </span>
                    </p>
                    </br>
                    <p id= "event-sp-time">
                        <strong>Time: </strong> <br>
                        <?= $detail['event']['startTime'] ?> <strong> to </strong> <?= $detail['event']['endTime'] ?>
                    </p>
                    </br>
                    <p id ="event-sp-venue">
                        <strong> Venue :</strong> </br>
                        <?= $detail['event']['venue'] ?>
                    </p>
                    </br>
                    <p id ="event-sp-loc" >
                        <strong> Location:</strong> </br>
                        <?= $detail['event']['location'] ?>
                    </p>
                    </br>        
                </div>
            </div>
        <?php endif; ?>



    </div>
</section>

<section id="event">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div id="event-carousel" class="carousel slide" data-interval="false">
                    <h2 class="heading">THE Performers</h2>
                    <a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Chester Bennington</h4>
                                        <h5>Vocal</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Mike Shinoda</h4>
                                        <h5>vocals, rhythm guitar</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Rob Bourdon</h4>
                                        <h5>drums</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event1.jpg" alt="event-image">
                                        <h4>Chester Bennington</h4>
                                        <h5>Vocal</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event2.jpg" alt="event-image">
                                        <h4>Mike Shinoda</h4>
                                        <h5>vocals, rhythm guitar</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Rob Bourdon</h4>
                                        <h5>drums</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="guitar">
                <img class="img-responsive" src="../images/evento/guitar.png" alt="guitar">
            </div>
        </div>			
    </div>
</section><!--/#event-->

<section id="explore">
    <?php if ($detail['event']['description'] != null): ?>

        <div class="container wow fadeInUp">
            <div class="row">
                <div class="watch">
                    <img class="img-responsive" src="../images/explore/watch.png" alt="">
                </div>				
                <div class="col-md-4 col-md-offset-2 col-sm-5">
                    <h2><?= $detail ['event']['name'] ?> starts in</h2>
                </div>				
                <div class="col-sm-7 col-md-6">					
                    <ul id="countdown">
                        <li>					
                            <span class="days time-font">00</span>
                            <p>days </p>
                        </li>
                        <li>
                            <span class="hours time-font">00</span>
                            <p class="">hours </p>
                        </li>
                        <li>
                            <span class="minutes time-font">00</span>
                            <p class="">minutes</p>
                        </li>
                        <li>
                            <span class="seconds time-font">00</span>
                            <p class="">seconds</p>
                        </li>				
                    </ul>
                </div>
            </div>
            <div class="cart">
                <a href="#"><i class="fa fa-shopping-cart"></i> <span>Purchase Tickets</span></a>
            </div>
        </div>
    <?php endif; ?>
</section><!--/#explore-->

<section id="sponsor">
    <div id="sponsor-carousel" class="carousel slide" data-interval="false">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-sm-10">
                    <h2>Sponsors</h2>			
                    <a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    <div class="carousel-inner">
                        <div class="item active">
                            <ul>

                                <?php foreach ($detail['sponsors'] as $sponsor): ?>

                                    <li><a href="#"> <?= Html::img($sponsor['logo'], ['class' => 'event-img']) ?></a></li>
                                <?php endforeach; ?>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor2.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor3.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor4.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor5.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor6.png" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor6.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor5.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor4.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor3.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor2.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor1.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>				
        </div>
        <div class="light">
            <img class="img-responsive" src="../images/light.png" alt="">
        </div>
    </div>

</section><!--/#sponsor-->

<section class="container wow fadeInUp">
    <div class="row">
        <div class="col-md-12">
            <div class="section-title">
                <h1>BUY TICKET</h1>
                <p>Don't miss out</p>
            </div>
        </div>
    </div>
  <style>
  .badge-notify{
    background:red;
    position:relative;
    top: -20px;
    right: 10px;
  }
  .my-cart-icon-affix {
    position: fixed;
    z-index: 999;
  }
  </style>
<div class="page-header">
    <h1>Tickets
      <div style="float: right; cursor: pointer;">
        <span class="glyphicon glyphicon-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span>
      </div>
    </h1>
</div>


</section>





<!-- .pricing-section -->
<section id="section-venue" class="section-wrapper  gray-bg">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h1>THE VENUE</h1>
                </div>
            </div>
        </div>
        <!-- /.row-->

        <div class="row">
            <div class="col-md-12">
                <div id="map"></div>


            </div>
        </div>
        <!-- .row-->

        <div class="row">
            <div class="col-md-12">
                <div class="section-title">
                    <h2>STAY INFORMED</h2>
                    <p>Don't miss this event!</p>
                </div>

            </div>
        </div>

    </div>
</section>


<?php
JSRegister::begin();
([
    'key' => 'step2_script',
        ]);
?>
<script>
    $("#activate-step-3").click(function (e) {
        e.preventDefault();
        var form = $('.step-2');
        var formData = form.serialize();

        $.ajax({
            url: '<?php echo \Yii::$app->getUrlManager()->createUrl("ticketpurchases/save") ?>',
            type: 'POST',
            data: formData,
            success: function (response)
            {

                console.log(response);
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        })
        return false;
    });
</script>
<?php JSRegister::end(); ?>