<?php
/* @var $this yii\web\View */

use consumer\assets\ConsumerAsset;
use yii\helpers\Html;
use thamtech\uuid\helpers\UuidHelper;
use thamtech\uuid\helpers\UuidValidator;
use yii\web\Session;
use richardfan\widget\JSRegister;
use yii\bootstrap\Modal;

ConsumerAsset::register($this);
/* @var $this yii\web\View */
$this->title = 'checkout';


$uuid = \thamtech\uuid\helpers\UuidHelper::uuid();

$validator = new \thamtech\uuid\validators\UuidValidator();
if ($validator->validate($uuid, $error)) {
    $consumerId = $uuid;
} else {
    // not valid
    echo ($error);
}
$session = Yii::$app->session;
?>

<?php
//echo(json_encode($session['checkout'])); exit
?>

<div class="row panel_check" >
    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
        <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 well text-center">
            <h1 class="text-center">Provide your details</h1>
            <div class="main-login main-center">
                <form  method="post" id="details_form" action="">
                    <div class="form-group check_1">
                        <label for="name" class="cols-sm-2 control-label">Name</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" id="name"  placeholder="Your Name" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group check_2">
                        <label for="email" class="cols-sm-2 control-label">Email</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="email" id="email"  placeholder="Your Email"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group check_3">
                        <label for="mobile" class="cols-sm-2 control-label required">Mobile</label>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="mobile" id="mobile"  placeholder="Your Mobile" value ="254"/>
                            </div>
                        </div>
                    </div>
                   <input type=submit id="proceed_btn" class="btn btn-primary btn-lg" value="Proceed"/>
                   
                </form>
            </div>
        </div>
   </div>

    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"></div>
</div>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        
<form method="post" action="https://www.jambopay.com/JPExpress.aspx" target="_blank" class="container">


    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-xs-12 col-sm-12 col-md-12 well text-center">
                <h3 class="text-center">Make payment</h3>

                <!--<form></form> --> 
                <div class="row">
                    <article class="col-lg-12 col-md-12">

                        <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button><strong><i class="icon-info-sign"></i> Information</strong><br>
                            You are about to make a payment for the following ticket items. Please confirm your order
                            </div
                            <div class="row col-xs-12 col-sm-12 col-md-12 custyle">
                                <table class="table table-bordered">

                                    <tr>
                                        <th>Ticket Name</th>
                                        <th>Unit Price</th>
                                        <th>Quantity</th>
                                        <th>Total</th>
                                    </tr>
                                   

                                


                                    <tr class="lead">
                                        <td colspan="3">Grand Total</td>
                                        <td>KSH  16000</td>
                                    </tr>
                                </table>

                            </div>

                            <input type="hidden" name="jp_item_type" value="cart"/>
                            <input type="hidden" name="jp_item_name" value="test shop"/>
                            <input type="hidden" name="order_id" value="455879"/>
                            <input type="hidden" name="jp_business" value="demo@webtribe.co.ke"/>
                            <input type="hidden" name="jp_amount_1" value="51"/>
                            <input type="hidden" name="jp_amount_2" value="0"/>
                            <input type="hidden" name="jp_amount_5" value="0"/>
                            <input type="hidden" name="jp_payee" value="email@yourcustomer.com"/>
                            <input type="hidden" name="jp_shipping" value="company name"/>
                            <input type="hidden" name="jp_rurl" value="http://www.yourwebsite.com/testpost/Result.aspx?ii=0"/>
                            <input type="hidden" name="jp_furl" value="http://www.yourwebsite.com/testpost/Result.aspx?ii=1"/>
                            <input type="hidden" name="jp_curl" value="http://www.yourwebsite.com/testpost/Result.aspx?ii=2"/>
                        





                             <button id="back_step2" class="btn btn-primary btn-lg">Back</button>
                            <button type="submit" class="btn btn-primary btn-md">Pay</button>



                        </div>
                    </div>
                </div>

            </form>

            <form class="container">

                <div class="row setup-content" id="step-4">
                    <div class="col-xs-12">
                        <div class="col-md-12 well text-center">
                            <h1 class="text-center"> STEP 4</h1>

                            <!--<form></form> -->                

                        </div>
                    </div>
                </div>

            </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>



<?php JSRegister::begin();
([
    'key' => 'step1_script',
   
]); 

?>
<script>
$("#proceed_btn").click(function(e){ 
     e.preventDefault();
  var form = $('#details_form');
  var  formData = form.serialize();
  

  
  console.log(formData);
          $.ajax({
            url    : 'create',
            type   : 'POST',
            data   : {'details':formData},
            dataType:'json',
            success: function (response) 
            {   
              
               console.log(response);
              
            },
            error: function (request, status, error) {
               //alert(request.responseText);
    }
        });
    return false;        
 });
</script>
<?php  JSRegister::end(); ?>
