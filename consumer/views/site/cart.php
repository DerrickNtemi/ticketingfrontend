<?php
/* @var $this yii\web\View */
use consumer\assets\CartAsset;
use consumer\assets\ConsumerAsset;
use yii\helpers\Html;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use richardfan\widget\JSRegister;

ConsumerAsset::register($this);
CartAsset::register($this);

function getColClass($items)
{
    $colClass='';
    $checker=count($items)%3;
    switch ($checker) {
        case 1:
            $colClass='col-md-offset-4';
            break;
        case 2:
            $colClass='col-md-offset-2';
            break;        
        
           
    }

    return $colClass;

}
$this->title = 'cart';
?>

<section id="home">
    <div class="wow fadeInUp">

        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                    <li><img src="../images/slider/bg1.jpg" alt="bg1"  id="wows1_0"/>
                        </li>
                    <li><img src="../images/slider/bg2.jpg" alt="slider"  id="wows1_1"/>
                        
                    </li>
                    <li><img src="../images/slider/bg3.jpg" alt="bg3" id="wows1_2"/>
                       
                    </li>
                </ul>
            </div>

        </div>	

    </div>
</section>

<?php //echo json_encode($details);
 

?> 
<section id="section-intro" class="section-wrapper about-event">
    <div class="container wow fadeInUp">
     <div class="row">
       <?php if($detail['event']['description']!=null):?>
        <div class=" col-xs-6  col-sm-6 col-md-6 offset-3 col-lg-6 evnt-des">
                <h3 id="ent-description">Event Description</h3>
                <p id= "description-p" class="lead">
                <?= $detail['event']['description'] ?>
                </p>
                </br>
                <p id="event-sp-date"> <strong>Date:</strong> <br> <?=$detail['event']['startDate']?> <strong>
                to </strong> <?= $detail['event']['endDate']?>
                <span id="calendar">
                <a href=""class="cal-link">
                <i class="fa fa-calendar"></i>
                <span class="cal"> Add to calendar</span>
                </a> 
                </span>
                </p>
                 </br>
               <p id= "event-sp-time">
                <strong>Time: </strong> <br>
                 <?= $detail['event']['startTime']?> <strong> to </strong> <?= $detail['event']['endTime']?>
                </p>
                </br>
                <p id ="event-sp-venue">
               <strong> Venue :</strong> </br>
                <?= $detail['event']['venue']?>
                </p>
               </br>
               <p id ="event-sp-loc" >
               <strong> Location:</strong> </br>
                <?= $detail['event']['location']?>
                </p>
               </br>        
            </div>
            </div>
            <?php endif;?>
           
        
    
    </div>
</section>

<section id="event">
    <div class="container wow fadeInUp">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <div id="event-carousel" class="carousel slide" data-interval="false">
                    <h2 class="heading">THE Performers</h2>
                    <a class="even-control-left" href="#event-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="even-control-right" href="#event-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Chester Bennington</h4>
                                        <h5>Vocal</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Mike Shinoda</h4>
                                        <h5>vocals, rhythm guitar</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Rob Bourdon</h4>
                                        <h5>drums</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event1.jpg" alt="event-image">
                                        <h4>Chester Bennington</h4>
                                        <h5>Vocal</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event2.jpg" alt="event-image">
                                        <h4>Mike Shinoda</h4>
                                        <h5>vocals, rhythm guitar</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single-event">
                                        <img class="img-responsive" src="../images/evento/event3.jpg" alt="event-image">
                                        <h4>Rob Bourdon</h4>
                                        <h5>drums</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="guitar">
                <img class="img-responsive" src="../images/evento/guitar.png" alt="guitar">
            </div>
        </div>			
    </div>
</section><!--/#event-->

<section id="explore">
<?php if($detail['event']['description']!=null):?>

    <div class="container wow fadeInUp">
        <div class="row">
            <div class="watch">
                <img class="img-responsive" src="../images/explore/watch.png" alt="">
            </div>				
            <div class="col-md-4 col-md-offset-2 col-sm-5">
                <h2><?= $detail ['event']['name']?> starts in</h2>
            </div>				
            <div class="col-sm-7 col-md-6">					
                <ul id="countdown">
                    <li>					
                        <span class="days time-font">00</span>
                        <p>days </p>
                    </li>
                    <li>
                        <span class="hours time-font">00</span>
                        <p class="">hours </p>
                    </li>
                    <li>
                        <span class="minutes time-font">00</span>
                        <p class="">minutes</p>
                    </li>
                    <li>
                        <span class="seconds time-font">00</span>
                        <p class="">seconds</p>
                    </li>				
                </ul>
            </div>
        </div>
        <div class="cart">
            <a href="#"><i class="fa fa-shopping-cart"></i> <span>Purchase Tickets</span></a>
        </div>
    </div>
<?php endif;?>
</section><!--/#explore-->

<section id="sponsor">
    <div id="sponsor-carousel" class="carousel slide" data-interval="false">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-sm-10">
                    <h2>Sponsors</h2>			
                    <a class="sponsor-control-left" href="#sponsor-carousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                    <a class="sponsor-control-right" href="#sponsor-carousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
                    <div class="carousel-inner">
                        <div class="item active">
                            <ul>
                            
                            <?php  foreach($detail['sponsors'] as $sponsor):?>
                                
                                <li><a href="#"> <?= Html::img($sponsor['logo'], ['class' => 'event-img']) ?></a></li>
                        <?php endforeach;?>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor2.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor3.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor4.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor5.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor6.png" alt=""></a></li>
                            </ul>
                        </div>
                        <div class="item">
                            <ul>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor6.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor5.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor4.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor3.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor2.png" alt=""></a></li>
                                <li><a href="#"><img class="img-responsive" src="../images/sponsor/sponsor1.png" alt=""></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>				
        </div>
        <div class="light">
            <img class="img-responsive" src="../images/light.png" alt="">
        </div>
    </div>
    
</section><!--/#sponsor-->

                <section class="container wow fadeInUp">
                 <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>BUY TICKET</h1>
                        <p>Don't miss out</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tickets
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <!-- BEGIN PRODUCTS -->
                                 <?php foreach($detail['tickets'] as $key=>$ticket):?>
                                <div class="col-md-4 col-sm-6">
                                    <div class="sc-product-item thumbnail">
                                       
                                        <div class="caption pricing-head">
                                            <h4 data-name="product_name"><?= $ticket['ticketNameId']['name']?></h4>
                                            <p data-name="product_desc"></p>
                                            <strong class="price pull-left"> <span class="price">Kshs &nbsp;<?= $ticket['price']?></span></strong>

                                            <hr class="line">

                                            <div>
                                                
                                               
                                                <div class="form-group2">
                                                    <input class="sc-cart-item-qty" name="product_quantity" min="1" value="1" type="number">
                                                </div>
                                                

                                                <input name="product_price" value="<?= $ticket['price']?>" type="hidden" />
                                                <input name="product_id" value="<?=$ticket['id']?>" type="hidden" />
                                               
                                                <button class="sc-add-to-cart btn btn-success btn-sm pull-right fa fa-shopping-cart"> &nbsp; Buy ticket</button>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                 <?php endforeach;?>
                                <!-- END PRODUCTS -->
                            </div>

                        </div>
                    </div>

                </div>

               <aside class="col-md-4">
                
                <!-- Cart submit form -->
                <form action="https://www.jambopay.com/JPExpress.aspx" method="POST"> 
                    <!-- SmartCart element -->
                    <div id="smartcart"> </div>

                    
                </form>



                </form>
                
            </aside>
            </div>
        </section>



<?php JSRegister::begin();
([
    'key' => 'smartcartscript',
   
]); 

?>
<script>
          $(document).ready(function(){
            // Initialize Smart Cart        
            $('#smartcart').smartCart({
                                submitSettings: {
                                    submitType: 'ajax' // form, paypal, ajax
                                },
                                toolbarSettings: {
                                    checkoutButtonStyle: 'default' // default, paypal, image
                                }
                            });
        });
            </script>
<?php  JSRegister::end(); ?>
        

    <!-- .pricing-section -->
    <section id="section-venue" class="section-wrapper  gray-bg">
        <div class="container wow fadeInUp">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h1>THE VENUE</h1>
                    </div>
                </div>
            </div>
            <!-- /.row-->

            <div class="row">
                <div class="col-md-12">
                    <div id="map"></div>

    
                </div>
            </div>
            <!-- .row-->

            <div class="row">
                <div class="col-md-12">
                    <div class="section-title">
                        <h2>STAY INFORMED</h2>
                        <p>Don't miss this event!</p>
                    </div>
                   
                </div>
            </div>

        </div>
    </section>
    

