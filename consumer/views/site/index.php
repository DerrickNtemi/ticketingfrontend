<?php
use yii\helpers\Html;
use consumer\assets\ConsumerAsset;
ConsumerAsset::register($this);
?>



        <!-- Carousel
================================================== -->
  <section id="home">
  <div class="wow fadeInUp">

        <div id="wowslider-container1">
            <div class="ws_images"><ul>
                    <li><img src="images/slider/bg1.jpg" alt="bg1"  id="wows1_0"/>
                        
                    <li><img src="images/slider/bg2.jpg" alt="slider"  id="wows1_1"/>
                        
                    </li>
                    <li><img src="images/slider/bg3.jpg" alt="bg3" id="wows1_2"/>
                        
                    </li>
                </ul>
            </div>

        </div>	

    </div>
        </section>
 
        <!--/#home-->


        <div class="container-fluid">
            <div id="searchForm-holder"  class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                    <div id="slide-slice1" class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" id="searchForm">

                        <div class="row">

                            <h3 id="search-title">Search Event</h3>
                            <div class="section-title-divider"></div>
                            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                                <div class="input-group buscador-principal">    					
                                    <input name="search_param" value="all" id="search_param" type="hidden">         
                                    <input class="form-control" name="x" placeholder="Search Events" type="text">

                                    <span class="input-group-btn">
                                        <button class="btn btn-primary" type="button"> <span class="glyphicon glyphicon-search icon-search"></span>Search</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="slide-slice2" class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                    </div>


                </div>
            </div>
        </div>

        <!--==========================
         Upcoming events Section
        ============================-->
        <section id="upcoming"> 
            <div class="container wow fadeInUp">
			  <?php //echo json_encode($events)
?>
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="section-title animated lightSpeedIn">Upcoming Events</h3>

                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title-divider"></div>
                </div>

                <div class="row" id="events-row">
					<?php foreach($events as $key=>$event):?>
					
					
                    <div class= "col-xs-12 col-sm-12 col-md-4 col-lg-4">
					
                        <div class="row">
                            <div class= "col-xs-12 col-sm-12 col-md-12 col-lg-11" id="events-card">

                                <div class="row event-title-holder">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                                
                                        <h1 id="event-title"><?=$event['name']?></h1>
                                    </div>
                                </div>
                                <div class="row event-title-line">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 event-divider"></div>
                                </div>
                                <div class="row event-content">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">   
                                        <p>
                                            <?= Html::img($event['poster'], ['class' => 'event-img']) ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="event-info" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 event-date">
                                            <div class="row event-date-holder">
                                                <span id="event-date-text"><?=$event['startDate']?></span>
												<div class="row conjuctionDateHolder">
												<span id="conjuctionDate">to</span>
												</div>
												<span id="endDate">
                                                <?= $event['endDate']?> 
                                                </span>
                                            </div>
                                             
                                        </div>
                                        <div class="col-xs- col-sm-8 col-md-8 col-lg-8">
                                            <div class="row event-time-holder">
                                                <span class="event-time"><?= $event['startTime']?> to <?= $event['endTime']?></span>
                                            </div>
                                            <div class="event-venue-holder">
                                                <span class="event-venue"> <?= $event['venue']?></br><?=$event ['location']?></span> </br>
								
                                            </div>
											<div class=row viewevent>
											<?= Html::a('<i class="fa fa-fw fa-eye pull-right"></i>',  ['test','id'=>$event['id']])?>
											</div>
									
                                            <div class="row share-btn-holder">
                                                <a class="pull-right share-btn" href=""><i class="fa fa-share-alt-square share-icon"></i></a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					<?php endforeach;?>
                    </div>
            </div>

        </section>

        <section id="explore">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="watch">
                        <img class="img-responsive" src="../images/explore/watch.png" alt="">
                    </div>				
                    <div class="col-md-4 col-md-offset-2 col-sm-5">
                        <h2>our next event in</h2>
                    </div>				
                    <div class="col-sm-7 col-md-6">					
                        <ul id="countdown">
                            <li>					
                                <span class="days time-font">00</span>
                                <p>days </p>
                            </li>
                            <li>
                                <span class="hours time-font">00</span>
                                <p class="">hours </p>
                            </li>
                            <li>
                                <span class="minutes time-font">00</span>
                                <p class="">minutes</p>
                            </li>
                            <li>
                                <span class="seconds time-font">00</span>
                                <p class="">seconds</p>
                            </li>				
                        </ul>
                    </div>
                </div>
                <div class="cart">
                    <a href="#"><i class="fa fa-shopping-cart"></i> <span>Purchase Tickets</span></a>
                </div>
            </div>
        </section><!--/#explore-->

        <section id="help">
            <div class="container wow fadeInUp">


                <div class="col-md-12">
                    <h3 class="section-title animated lightSpeedIn">How it works</h3>

                </div>


                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title-divider"></div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="main_feature text-center">

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                        <i class="glyphicon glyphicon-hand-right"></i>
                                    </div>

                                    <h4>STEP 1</h4>
                                    <div class="separator3"></div>
                                    <p> Lorem Ipsum is simply dummy text of the printing and typesetting let. 
                                        Lorem Ipsum has been the industry.</p>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                        <i class="glyphicon glyphicon-hand-right"></i>
                                    </div>

                                    <h4>STEP 2</h4>
                                    <div class="separator3"></div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting let. 
                                        Lorem Ipsum has been the industry.</p>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                        <i class="glyphicon glyphicon-hand-right"></i>
                                    </div>
                                    <h4>STEP 3 </h4>
                                    <div class="separator3"></div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting let. 
                                        Lorem Ipsum has been the industry.</p>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <div class="single_feature">
                                    <div class="single_feature_icon">
                                        <i class="glyphicon glyphicon-hand-right"></i>
                                    </div>

                                    <h4>STEP 4 </h4>
                                    <div class="separator3"></div>
                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting let. 
                                        Lorem Ipsum has been the industry.</p>
                                </div>
                            </div>

                        </div>
                    </div>
                </div><!--End of container -->
            </div>

        </section>

        <section id="contact" class="contact">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
                        <div class="contact_contant sections">
                            <div class="row">
                                <div class="col-md-12">
                                    <h3 class="section-title animated lightSpeedIn">Keep in touch</h3>

                                </div>

                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 section-title-divider"></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="main_contact_info">
                                        <div class="row">
                                            <div class="contact_info_content padding-top-90 padding-bottom-60 p_l_r">
                                                <div class="col-sm-12">
                                                    <div class="single_contact_info">
                                                        <div class="single_info_text">
                                                            <h3>OUR ADDRESS</h3>
                                                            <h4>kenyatta lane 
                                                                Nairobi</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="single_contact_info">
                                                        <div class="single_info_text">
                                                            <h3>CALL US</h3>
                                                            <h4>+254706523894</h4>
                                                            <h4>020788955</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="single_contact_info">
                                                        <div class="single_info_text">
                                                            <h3>EMAIL US</h3>
                                                            <h4>contactus@email.com</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of contact section -->
