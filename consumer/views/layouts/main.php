<?php
/* @var $this \yii\web\View */
/* @var $content string */

use consumer\assets\ConsumerAsset;
use yii\helpers\Html;

ConsumerAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>    </head>

    <?php $this->head() ?>
    <body>
<!--        <?php $this->beginBody() ?>
        <header id="header" role="banner">		
            <div class="main-nav">
                <div class="container">

                    <div class="row">	        		
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar wow flipInX"></span>
                                <span class="icon-bar  wow flipInX"></span>
                                <span class="icon-bar wow flipInX"></span>
                            </button>
                            <a class="navbar-brand" href="#hero">
                                //logo
                            </a>                    
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">                 
                                <li class="page-scroll active"><a href="#home">Home</a></li>

                                <li class="page-scroll"><a href="#upcoming">Upcoming Events</a></li>
                                <li class="page-scroll"><a href="#explore">Explore</a></li> 
                                <li class="page-scroll"><a href="#help">How it works</a></li> 
                                <li><a class="no-scroll" href="ticketing/consumer/views/site/event.php">PURCHASE TICKETS</a></li>
                                <li class="page-scroll"><a href="#contact">Contact</a></li>       
                            </ul>
                        </div>
                    </div>
                </div>
            </div>                    
        </header>-->
      
        <?= $content?>
		
    <!--?= Yii::$app->view->renderFile('@consumer/views/site/event.php');?-->
        <!--Footer section-->
        <section class="footer">
            <div class="container wow fadeInUp">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="main_footer">
                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="flowus">
                                        <a href=""><i class="fa fa-facebook icon-format"></i></a>
                                        <a href=""><i class="fa fa-twitter icon-twitter"></i></a>
                                        <a href=""><i class="fa fa-google-plus"></i></a>
                                        <a href=""><i class="fa fa-instagram"></i></a>
                                        <a href=""><i class="fa fa-youtube"></i></a>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="copyright_text">
                                        <p class=" wow fadeInRight" data-wow-duration="1s">All Rights Reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End off footer Section-->

        <?php $this->endBody() ?>
        //<?php
//$script = <<< JS
//<!--Start of Tawk.to Script-->
//var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
//(function(){
//var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
//s1.async=true;
//s1.src='https://embed.tawk.to/5923f1508028bb73270474cf/default';
//s1.charset='UTF-8';
//s1.setAttribute('crossorigin','*');
//s0.parentNode.insertBefore(s1,s0);
//})();
//
//JS;
//$this->registerJs($script);
//?>
<!--End of Tawk.to Script-->
    </body>
</html>
<?php $this->endPage() ?>
