<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketpurchasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ticketpurchases';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticketpurchases-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Ticketpurchases', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'purchaseNumber',
            'eventsId',
            'eventTicketsId',
            'consumerId',
            // 'numberOfTickets',
            // 'ticketAmount',
            // 'totalAmount',
            // 'amountPaid',
            // 'balance',
            // 'createdBy',
            // 'createdAt',
            // 'statusId',
            // 'verifiiedAt',
            // 'verifiedBy',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
