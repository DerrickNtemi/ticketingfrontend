<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ticketpurchases */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticketpurchases-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'purchaseNumber')->textInput() ?>

    <?= $form->field($model, 'eventsId')->textInput() ?>

    <?= $form->field($model, 'eventTicketsId')->textInput() ?>

    <?= $form->field($model, 'consumerId')->textInput() ?>

    <?= $form->field($model, 'numberOfTickets')->textInput() ?>

    <?= $form->field($model, 'ticketAmount')->textInput() ?>

    <?= $form->field($model, 'totalAmount')->textInput() ?>

    <?= $form->field($model, 'amountPaid')->textInput() ?>

    <?= $form->field($model, 'balance')->textInput() ?>

    <?= $form->field($model, 'createdBy')->textInput() ?>

    <?= $form->field($model, 'createdAt')->textInput() ?>

    <?= $form->field($model, 'statusId')->textInput() ?>

    <?= $form->field($model, 'verifiiedAt')->textInput() ?>

    <?= $form->field($model, 'verifiedBy')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
