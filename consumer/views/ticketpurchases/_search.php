<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TicketpurchasesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ticketpurchases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'purchaseNumber') ?>

    <?= $form->field($model, 'eventsId') ?>

    <?= $form->field($model, 'eventTicketsId') ?>

    <?= $form->field($model, 'consumerId') ?>

    <?php // echo $form->field($model, 'numberOfTickets') ?>

    <?php // echo $form->field($model, 'ticketAmount') ?>

    <?php // echo $form->field($model, 'totalAmount') ?>

    <?php // echo $form->field($model, 'amountPaid') ?>

    <?php // echo $form->field($model, 'balance') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'statusId') ?>

    <?php // echo $form->field($model, 'verifiiedAt') ?>

    <?php // echo $form->field($model, 'verifiedBy') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
