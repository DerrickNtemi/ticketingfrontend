<?php

namespace consumer\assets;

use yii\web\AssetBundle;


class CartAsset extends AssetBundle
{

    public $sourcePath = '@vendor/techlab/jquery-smartcart/dist/';
    public $css = [
        'css/smart_cart.min.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];

     public $js = [
     'js/jquery.smartCart.min.js'
     ];

}