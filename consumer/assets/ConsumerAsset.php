<?php

namespace consumer\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class ConsumerAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/animate.min.css',
        'bootstrap/css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
        'css/responsive.css',
        'css/style.mod.css',
        'css/events.css',
        'css/checkout.css',
        'css/lpage.css',
    ];
    public $js = [
        'js/jquery/jquery-3.2.1.min.js',
        'js/jquery/jquery-migrate.min.js',
        'js/jquery.nav.js',
        'js/jquery.parallax.js',
        'js/jquery.scrollTo.js',
        'js/custom.js',
        'js/easing/easing.js',
        'js/stickyjs/sticky.js',
        'js/wow/wow.min.js',
        'js/morphext/morphext.min.js',
        'js/superfish/superfish.min.js',
        'js/superfish/hoverIntent.js',
        'js/countdown-timer.js',
        'js/html5shiv.js',
        'js/main.js',
        'js/modernizr.js',
        'js/respond.min.js',
        'js/wowslider.js',
        'js/script.js',
        'js/jquery.mycart.min.js'
      
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        
    ];
}
