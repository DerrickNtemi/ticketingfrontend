<?php

namespace consumer\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Service;
//use common\models\Events;
use yii\web\Session;

/**
 * Site controller
 */
class SiteController extends Controller {
//    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'checkout','tablecart', 'error', 'index', 'lpage', 'cart', 'save', 'create','test'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        //echo ('found'); die();
        $service = new Service();
        $events = $service->getEvents();
        return $this->render('index', array(
                    'events' => $events
        ));
    }

    public function actionCart($id) {
        $service = new Service();
        $details = $service->getEventDetails($id);



        return $this->render('cart', array(
                    'detail' => $details,
        ));
    }

    public function actionCheckout() {
        return $this->render('checkout');
    }
     public function actionTicket() {
         
         
        return $this->render('test');
    }
   public function actionTablecart() {
         
         
        return $this->render('tablecart');
    }
    public function actionTest($id) {
        $service = new Service();
        $details = $service->getEventDetails($id);
        return $this->render('test', array(
                    'detail' => $details,
        ));
    }

    public function actionCreate() {
        $session = Yii::$app->session;

        $request = Yii::$app->request;

        $customer_detail = parse_str(filter_input(INPUT_POST, 'details'), $searcharray);



        $_SESSION['details'] = [];
        $_SESSION['details'] = [$searcharray];

        var_dump($_SESSION['details']);
        exit;

        }





        //var_dump($session['checkout']); exit;
        


        /**
         * Login action.
         *
         * @return string
         */
        public function actionLogin()
        {
        if (!Yii::$app->user->isGuest) {
        return $this->goHome();
        }
        $appName = "Merchant";
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('@common/views/login', [
                        'model' => $model,
                        'appName' => $appName
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSave() {
        $session = Yii::$app->session;

        $request = Yii::$app->request;
        $tickets_amount = filter_input(INPUT_POST, 'total_amount');

        // Get the results as JSON string
        $tickets_amount = filter_input(INPUT_POST, 'total_amount');
        $tickets_amount = substr($tickets_amount, 3, -3);
        $tickets_amount = preg_replace("/[^0-9]/", "", $tickets_amount);

        $tickets_amounts = json_decode($tickets_amount);


        $cart_list = filter_input(INPUT_POST, 'cart_list');

        // Convert JSON to array
        $cart_list = json_decode($cart_list);




        if (!isset($_SESSION['checkout'])) {
            $_SESSION['checkout'] = true;
        } else {
            $_SESSION['checkout'] = [];
            $_SESSION['checkout'] = [$cart_list, $tickets_amounts];
        }
    }

}
