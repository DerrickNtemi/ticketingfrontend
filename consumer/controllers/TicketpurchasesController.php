<?php

namespace consumer\controllers;

use Yii;
use app\models\Ticketpurchases;
use app\models\TicketpurchasesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TicketpurchasesController implements the CRUD actions for Ticketpurchases model.
 */
class TicketpurchasesController extends Controller
{

	public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ticketpurchases models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TicketpurchasesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ticketpurchases model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Ticketpurchases model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$session = Yii::$app->session;
		// open a session
		$session->open();
        
        $model = new Ticketpurchases();

        
        $request = Yii::$app->request;
		
		 $consumerId = isset($_POST['consumerId']) ? $_POST['consumerId'] : null;
       
        $numberOfTickets = isset($_POST['numberOfTickets']) ? $_POST['numberOfTickets'] : null;
         $ticketName= isset($_POST['ticketName']) ? $_POST['ticketName'] : null;
		 $eventTicketsId= isset($_POST['eventTicketsId']) ? $_POST['eventTicketsId'] : null;
		 $ticketAmount= isset($_POST['ticketAmount']) ? $_POST['ticketAmount'] : null;
		 $totalAmount= isset($_POST['totalAmount']) ? $_POST['totalAmount'] : null;
		 $amountPaid= isset($_POST['amountPaid']) ? $_POST['amountPaid'] : null;
		  
		 
         // check if a session is already open
        if ($session->isActive){
			
			$checkout=$session['checkout'];
			$ticket_select=[
            'numberOfTickets' => $numberOfTickets,
			 'ticketName' => $ticketName,
			  'eventTicketsId' => $eventTicketsId,
			   'ticketAmount' => $ticketAmount,
			    'totalAmount' => $totalAmount,
				 'amountPaid' => $amountPaid,];
				 
		
         $checkout[$consumerId] = $ticket_select;
		$session['checkout']=$checkout;
		
		
		 
		 
				 $myArray = json_encode($session['checkout']);
				 echo $myArray;
			
    }
	else{
		echo('Error');
	}
	}

    /**
     * Updates an existing Ticketpurchases model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ticketpurchases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ticketpurchases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Ticketpurchases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ticketpurchases::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
