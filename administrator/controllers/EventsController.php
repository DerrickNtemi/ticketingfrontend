<?php

namespace administrator\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Service;
use common\models\Events;
use common\models\EventTickets;
use common\models\EventGallery;
use common\models\EventSponsor;
/**
 * Site controller
 */
class EventsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $data = $service->getEvents();
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('index', ['data' => $events]);
    }

    public function actionPending() {
        $service = new Service();
        $data = $service->getEvents(7);
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('pending', ['data' => $events]);
    }

    public function actionActive() {
        $service = new Service();
        $data = $service->getActiveEvents();
        $events = [];
        foreach ($data as $dt) {
            $model = new Events();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $events[] = $model;
        }
        return $this->render('active', ['data' => $events]);
    }

    public function actionPublish($id) {
        $model = new Events();
        $service = new Service();
        $eventDetails = $service->getEventComprehensive($id);
        $model->setAttributes($eventDetails['event']);
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->publishEvent($model->id, $model->commission);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Event sucessfully published.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('publish', ['model' => $model]);
    }

    public function actionCancel($id) {
        $model = new Events();
        $service = new Service();
        $eventDetails = $service->getEventComprehensive($id);
        $model->setAttributes($eventDetails['event']);
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->cancelEvent($model->id, $model->commission);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Event sucessfully cancelled.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('cancel', ['model' => $model]);
    }

    public function actionView($id) {
        $model = new Events();
        $service = new Service();
        $eventDetails = $service->getEventComprehensive($id);
        $eventTickets = $eventGalleries = $eventSponsors = [];
        $model->setAttributes($eventDetails['event']);
        foreach ($eventDetails['tickets'] as $eventTicket) {
            $ticketsModel = new EventTickets();
            $ticketsModel->setAttributes($eventTicket);
            $eventTickets[] = $ticketsModel;
        }

        foreach ($eventDetails['gallery'] as $data) {
            $dataModel = new EventGallery();
            $dataModel->setAttributes($data);
            $eventGalleries[] = $dataModel;
        }

        foreach ($eventDetails['sponsors'] as $data) {
            $dataModel = new EventSponsor();
            $dataModel->setAttributes($data);
            $eventSponsors[] = $dataModel;
        }
        return $this->render('view', [
                    'model' => $model,
                    'tickettiers' => $eventTickets,
                    'sponsors' => $eventSponsors,
                    'gallery' => $eventGalleries,
                    'visibility' => false,
        ]);
    }

}
