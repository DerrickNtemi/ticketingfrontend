<?php

namespace administrator\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Service;
use common\models\UserAccount;

/**
 * Site controller
 */
class UsersController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $identity = Yii::$app->user->getIdentity();
        $usersData = $service->getAllUsers("",1);
        $users = [];
        foreach ($usersData as $data) {
            if($identity->id == $data['id']){
                continue;
            }
            $user = new UserAccount();
            $user->setAttributes($data);
            $users[] = $user;
        }
        return $this->render('index', ['users' => $users]);
    }

    public function actionCreate() {
        $model = new UserAccount();
        if ($model->load(Yii::$app->request->post())) {
            $model->password = $model->phoneNumber;
            $model->confirmPassword = $model->phoneNumber;
            $model->userRolesId = 5;
            $model->userTypesId = 3;
            if ($model->validate()) {
                $service = new Service();
                $response = (Object) $service->createUserAccount($model);
                if ($response->status['code'] == 100) {
                    \Yii::$app->getSession()->setFlash('success-message', "User account sucessfully created. Check user email for activation details");
                    return $this->redirect(['index']);
                } else {
                    $model->addError("", $response->status['message']);
                }
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionDelete($id) {
        $service = new Service();
        $response = (Object) $service->deleteUser($id);
        if ($response->status['code'] == 100) {
            \Yii::$app->getSession()->setFlash('success-message', "User account sucessfully deleted.");
        } else {
            \Yii::$app->getSession()->setFlash('fail-message', $response->status['message']);
        }
        return $this->redirect(['index']);
    }

}
