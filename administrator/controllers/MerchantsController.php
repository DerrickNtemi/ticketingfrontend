<?php

namespace administrator\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use common\models\Service;
use common\models\Merchants;

/**
 * Site controller
 */
class MerchantsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        $service = new Service();
        $data = $service->getMerchants(1);
        $merchants = [];
        foreach ($data as $dt) {
            $model = new Merchants();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $merchants[] = $model;
        }
        return $this->render('index', ['data' => $merchants]);
    }

    public function actionPending() {
        $service = new Service();
        $data = $service->getMerchants(4);
        $merchants = [];
        foreach ($data as $dt) {
            $model = new Merchants();
            $model->setAttributes($dt);
            $model->createdAt = $service->timestampToString1($model->createdAt);
            $merchants[] = $model;
        }
        return $this->render('pending', ['data' => $merchants]);
    }

    public function actionApprove($id) {
        $model = new Merchants();
        $service = new Service();
        $model->setAttributes($service->getMerchant($id));
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->merchantAccountVerification($model,1);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Account verification was successful.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('verify', ['model' => $model]);
    }

    public function actionCancel($id) {
        $model = new Merchants();
        $service = new Service();
        $model->setAttributes($service->getMerchant($id));
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->merchantAccountVerification($model,2);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Account verification was successful.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('reject', ['model' => $model]);
    }

    public function actionUpdate($id) {
        $model = new Merchants();
        $service = new Service();
        $model->setAttributes($service->getMerchant($id));
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->merchantAccountUpdating($model);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Account sucessfully verified.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete($id) {
        $model = new Merchants();
        $service = new Service();
        $model->setAttributes($service->getMerchant($id));
        if ($model->load(Yii::$app->request->post())) {
            $service = new Service();
            $response = (Object) $service->merchantAccountDeletion($model);
            if ($response->status['code'] == 100) {
                \Yii::$app->getSession()->setFlash('success-message', "Account sucessfully deleted.");
                return $this->redirect(['view', 'id' => $id]);
            } else {
                $model->addError("", $response->status['message']);
            }
        }
        return $this->render('delete', ['model' => $model]);
    }

    public function actionView($id) {
        $model = new Merchants();
        $service = new Service();
        $model->setAttributes($service->getMerchant($id));
        $model->createdAt = $service->timestampToString1($model->createdAt);
        $model->verifiedAt = $service->timestampToString1($model->verifiedAt);
        return $this->render('view', ['model' => $model]);
    }

}
