<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Cancel Event';
$this->params['breadcrumbs'][] = "Cancel [" . $model->name . "]";
?>
<div class="site-login" style="margin-bottom: 50px;">
    <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
        <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('success-message')): ?>
        <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12 text-right">
            <p style="border-bottom: 1px solid #f5f5f5; padding-bottom: 5px;">
                <?= Html::a('<span class="glyphicon glyphicon-list"></span> Active Events List', ['active'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-success', 'title' => Yii::t('yii', 'Update Event'),]); ?>
                <?= Html::a('<span class="glyphicon glyphicon-list"></span> Awaiting Publication List', ['pending'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-primary', 'title' => Yii::t('yii', 'Update Event'),]); ?>
                <?= Html::a('<span class="glyphicon glyphicon-list"></span> Events List', ['index'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-info', 'title' => Yii::t('yii', 'Update Event'),]); ?>
            </p>
        </div>
    </div>
    <div class="row" >
        <div id="login-container" style="float: none; ">
            <?php
            $form = ActiveForm::begin(['id' => 'publish-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'remarks', ['inputOptions' => ['placeholder' => 'remarks']])->textarea(['autofocus' => true])->label() ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Cancel Event', ['class' => 'btn btn-danger', 'name' => 'login-button', 'style' => 'width:100%;']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
