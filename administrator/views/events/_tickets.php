<?php

use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
if (!isset($visibility)) {
    $visibility = true;
}
?>
<?=

GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $tickets]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Ticket Type',
            'value' => function($data) {
                return $data->ticketTypeId['name'];
            }
        ],
        [
            'label' => 'Ticket Name',
            'value' => function($data) {
                return $data->ticketNameId['name'];
            }
        ],
        [
            'label' => 'Price',
            'value' => function($data) {
                return number_format($data->price);
            }
        ],
        [
            'label' => 'Quantity',
            'value' => function($data) {
                return number_format($data->ticketQuantity);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}&nbsp;&nbsp;',
            'visible' => $visibility,
            'buttons' => [
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['events/delete-ticket-tier', 'id' => $model->id, 'eventsId' => $model->eventsId['id']]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['data-value' => '#ticket-details-success-message', 'data-div' => '#ticket-tier-details', 'data-whatever' => $url, 'data-toggle' => "modal", 'data-target' => "#DeleteInnerReloadModal", 'title' => Yii::t('yii', 'Delete'),]);
                },
            ],
        ],
    ],
]);
?>
