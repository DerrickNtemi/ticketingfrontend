<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider([ 'allModels' => $data]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label'=>'Name',
            'value'=>function($data){
                return $data->name;
            }
        ],
        [
            'label'=>'Start Date',
            'value'=>function($data){
                return $data->startDate." ".$data->startTime;
            }
        ],
        [
            'label'=>'End Date',
            'value'=>function($data){
                return $data->endDate." ".$data->endTime;
            }
        ],
        [
            'label'=>'Merchant',
            'value'=>function($data){
                return $data->merchantId['businessName'];
            }
        ],
        [
            'label'=>'Status',
            'value'=>function($data){
                return $data->statusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{publish}&nbsp;&nbsp;{cancel}&nbsp;&nbsp;',
            'buttons' => [
                'view' => function($url, $model) {
                    $url = Url::toRoute(['events/view', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View'),]);
                },
                'publish' => function($url, $model) {
                    $url = Url::toRoute(['events/publish', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Publish'), 'class'=>$model->statusId['id'] == 10 ? "disable-merchant-verification" : "enable-merchant-verification"]);
                },
                'cancel' => function($url, $model) {
                    $url = Url::toRoute(['events/cancel', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Cancel')]);
                },
                    ],
                ],
            ],
        ]);
        ?>