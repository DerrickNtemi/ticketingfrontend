<?php
$this->title = Yii::t('app', 'Merchant Details');
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html;
?>
<div class="row members-index">
    <div class="col-md-12">
        <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
            <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
        <?php endif; ?>
        <?php if (Yii::$app->session->hasFlash('success-message')): ?>
            <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12 text-right">
                <p>
                    <?= Html::a('<span class="glyphicon glyphicon-ok"></span> Approve', ['approve', 'id' => $model->id], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-success', 'title' => Yii::t('yii', 'Authorize'),]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-remove"></span> Reject', ['cancel', 'id' => $model->id], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-danger', 'title' => Yii::t('yii', 'Cancel Event'),]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-list"></span> Merchant List', ['index'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-primary', 'title' => Yii::t('yii', 'New Event'),]); ?>
                    <?= Html::a('<span class="glyphicon glyphicon-list"></span> Awaiting Verification List', ['pending'], ['style' => 'margin-right:10px;', 'class' => 'btn btn-sm btn-info', 'title' => Yii::t('yii', 'Update Event'),]); ?>
                </p>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Merchant</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">Merchant Profile</div>
                            <div class="panel-body">
                                <table class="table table-striped profile-table">
                                    <tr>
                                        <th>Business Name</th>
                                        <th>:</th>
                                        <td><?= $model->businessName ?></td>
                                    </tr>
                                    <tr>
                                        <th>Town/City</th>
                                        <th>:</th>
                                        <td><?= $model->town ?></td>
                                    </tr>
                                    <tr>
                                        <th>Country</th>
                                        <th>:</th>
                                        <td><?= $model->countryId['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Business Address</th>
                                        <th>:</th>
                                        <td><?= $model->businessAddress ?></td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <th>:</th>
                                        <td><?= $model->phoneNumber ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <th>:</th>
                                        <td><?= $model->email ?></td>
                                    </tr>
                                    <tr>
                                        <th>Business Url</th>
                                        <th>:</th>
                                        <td><?= $model->businessUrl ?></td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <th>:</th>
                                        <td><?= $model->statusId['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Timestamp</th>
                                        <th>:</th>
                                        <td><?= $model->createdAt ?></td>
                                    </tr>
                                    <tr>
                                        <th>Verified By</th>
                                        <th>:</th>
                                        <td><?= $model->verifiedBy['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <th>Verified At</th>
                                        <th>:</th>
                                        <td><?= $model->verifiedAt ?></td>
                                    </tr>
                                    <tr>
                                        <th>Remarks</th>
                                        <th>:</th>
                                        <td><?= $model->remarks ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="panel panel-default">
                            <div class="panel-heading">Contact Person</div>
                            <div class="panel-body">
                                <table class="table table-striped profile-table">
                                    <tr>
                                        <th>Name</th>
                                        <th>:</th>
                                        <td><?= $model->contactPersonFirstName." ".$model->contactPersonMiddleName." " .$model->contactPersonLastName ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <th>:</th>
                                        <td><?= $model->contactPersonEmail ?></td>
                                    </tr>
                                    <tr>
                                        <th>Phone</th>
                                        <th>:</th>
                                        <td><?= $model->contactPersonPhoneNumber ?></td>
                                    </tr>
                                    <tr>
                                        <th>Position</th>
                                        <th>:</th>
                                        <td><?= $model->contactPersonPosition ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

