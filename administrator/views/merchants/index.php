<?php
$this->title = Yii::t('app', 'Merchant List');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?=$this->title?></div>
            <div class="panel-body grid-font-formating">
                <?= $this->render('_data', [ 'data'=>$data]) ?>
            </div>
        </div>
    </div>
</div>

