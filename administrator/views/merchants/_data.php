<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider([ 'allModels' => $data]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label'=>'Business Name',
            'value'=>function($data){
                return $data->businessName;
            }
        ],
        [
            'label'=>'Email',
            'value'=>function($data){
                return $data->email;
            }
        ],
        [
            'label'=>'Phone',
            'value'=>function($data){
                return $data->phoneNumber;
            }
        ],
        [
            'label'=>'Timestamp',
            'value'=>function($data){
                return $data->createdAt;
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view}&nbsp;&nbsp;{approve}&nbsp;&nbsp;{reject}&nbsp;&nbsp;{delete}&nbsp;&nbsp;',
            'buttons' => [
                'view' => function($url, $model) {
                    $url = Url::toRoute(['merchants/view', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, ['title' => Yii::t('yii', 'View'),]);
                },
                'approve' => function($url, $model) {
                    $url = Url::toRoute(['merchants/approve', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, ['title' => Yii::t('yii', 'Verify'), 'class'=>$model->statusId['id'] == 1 ? "disable-merchant-verification" : "enable-merchant-verification"]);
                },
                'reject' => function($url, $model) {
                    $url = Url::toRoute(['merchants/cancel', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Reject')]);
                },
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['merchants/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('yii', 'Delete'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>