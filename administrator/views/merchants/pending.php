<?php
$this->title = Yii::t('app', 'Merchant Awaiting Verification');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row members-index">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading"><?= $this->title ?></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 grid-font-formating">
                        <?= $this->render('_data', ['data' => $data]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

