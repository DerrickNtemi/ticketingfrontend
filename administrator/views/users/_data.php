<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
?>
<?=
GridView::widget([
    'dataProvider' => new ArrayDataProvider([ 'allModels' => $data]),
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label'=>'Name',
            'value'=>function($data){
                return $data->name;
            }
        ],
        [
            'label'=>'Username',
            'value'=>function($data){
                return $data->email;
            }
        ],
        [
            'label'=>'Last Login',
            'value'=>function($data){
                return $data->lastLogin;
            }
        ],
        [
            'label'=>'Status',
            'value'=>function($data){
                return $data->statusId['name'];
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}&nbsp;&nbsp;',
            'buttons' => [
                'delete' => function($url, $model) {
                    $url = Url::toRoute(['users/delete', 'id' => $model->id]);
                    return Html::a('<span class="glyphicon glyphicon-remove"></span>', $url, ['title' => Yii::t('yii', 'Delete'),]);
                },
                    ],
                ],
            ],
        ]);
        ?>