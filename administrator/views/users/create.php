<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = 'Account Creation';
$this->params['breadc rumbs'][] = $this->title;
?>
<div class="site-login" style="margin-bottom: 50px;">
    <div class="panel panel-default">
        <div class="panel-heading"><?= $this->title ?></div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $form = ActiveForm::begin(['id' => 'account-creation-form']);
                    if ($model->hasErrors()) {
                        echo $form->errorSummary($model, ['header' => '']);
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'name', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']], 'inputOptions' => ['placeholder' => 'Name']])->textInput()->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'email', ['addon' => ['prepend' => ['content' => '@']], 'inputOptions' => ['placeholder' => 'Email']])->textInput()->label() ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <?= $form->field($model, 'phoneNumber', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-phone"></span>']], 'inputOptions' => ['placeholder' => 'Phone Number']])->textInput()->label() ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= Html::submitButton('Create Account', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
