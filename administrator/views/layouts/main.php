<?php

/* @var $this \yii\web\View */
/* @var $content string */

use administrator\assets\AdminAsset;
use kartik\sidenav\SideNav;

AdminAsset::register($this);
$this->beginBlock('appName');
echo "Administrator Portal";
$this->endBlock();
?>
<?php $this->beginBlock('menu') ?>
<?php if (!Yii::$app->user->isGuest): ?>
    <?=

    SideNav::widget([
        'type' => SideNav::TYPE_INFO,
        'heading' => '',
        'items' => [
            [
                'label' => 'Dashboard',
                'icon' => 'dashboard',
                'url' => ['site/index'],
            ],
            [
                'label' => 'Merchants',
                'icon' => 'th',
                'url' => '#',
                'items' => [
                    ['label' => 'Merchant List', 'icon' => '', 'url' => ['merchants/index']],
                    ['label' => 'Merchants Awaiting Verification', 'icon' => '', 'url' => ['merchants/pending']],
                ],
            ],
            [
                'label' => 'Events',
                'icon' => 'list',
                'url' => '#',
                'items' => [
                    ['label' => 'Active Events', 'icon' => '', 'url' => ['events/active']],
                    ['label' => 'Events Awaiting Publication', 'icon' => '', 'url' => ['events/pending']],
                    ['label' => 'Events List', 'icon' => '', 'url' => ['events/index']],
                ],
            ],
            [
                'label' => 'Users',
                'icon' => 'user',
                'url' => '#',
                'items' => [
                    ['label' => 'Users List', 'icon' => '', 'url' => ['users/index']],
                    ['label' => 'Create', 'icon' => '', 'url' => ['users/create']],
                ],
            ],
            [
                'label' => 'Reports',
                'icon' => 'folder-open',
                'url' => '#',
                'items' => [
                    ['label' => 'Events List', 'icon' => '', 'url' => '#'],
                    ['label' => 'Revenue', 'icon' => '', 'url' => '#'],
                ],
            ],
        ],
    ]);
    ?>
<?php endif; ?>
<?php $this->endBlock() ?>
<?= $this->render('@common/views/layouts/main', ['content' => $content]) ?>