<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@consumer', dirname(dirname(__DIR__)) . '/consumer');
Yii::setAlias('@cooperate', dirname(dirname(__DIR__)) . '/cooperate');
Yii::setAlias('@merchants', dirname(dirname(__DIR__)) . '/merchants');
Yii::setAlias('@administrator', dirname(dirname(__DIR__)) . '/administrator');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');