<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" >
    <div class="row" >
        <div id="login-container" style="float: none;">
            <h1 align="center" id="form-title">CHANGE PASSWORD</h1>
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'change-password-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>

            <?= $form->field($model, 'currentPassword', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-lock"></span>']], 'inputOptions' => ['placeholder' => 'current password']])->passwordInput(['autofocus' => true]) ?>
            <?= $form->field($model, 'newPassword', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-lock"></span>']], 'inputOptions' => ['placeholder' => 'new password']])->passwordInput() ?>
            <?= $form->field($model, 'confirmPassword', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-lock"></span>']], 'inputOptions' => ['placeholder' => 'Confirm password']])->passwordInput() ?>

            <div class="row">
                <div class="col-md-5"><?= Html::submitButton('Change Password', ['class' => 'btn btn-primary', 'style' => 'border-radius:0px;']) ?></div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
