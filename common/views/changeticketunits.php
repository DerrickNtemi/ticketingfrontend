<?php 
use yii\helpers\Html;
?>
<p id="reload-div" class="hidden"></p>
<p id="change-url" class="hidden"></p>
<p id="success-div" class="hidden"></p>
<div class="row">
    <div class="col-md-6">
New Ticket Units : <input type="text" name="ticketunits" id="ticketunits" placeholder="New Ticket Units" class="form-control" />
    </div>
</div>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'form processing']) ?>
</div>
<div class="form-group">
    <div class="alert alert-danger"  id='error-summary' role="alert" style="display: none;"> </div>
</div>
<div class="clearfix"></div>