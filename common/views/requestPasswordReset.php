<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" >
    <div class="row" >
        <div id="login-container" style="float: none;">
            <h1 align="center" id="form-title">REQUEST PASSWORD RESET  </h1>
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'reset-password-request-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <div class="row"><div class="col-md-12">
                    <?= $form->field($model, 'username', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']], 'inputOptions' => ['placeholder' => 'username']])->textInput(['autofocus' => true]) ?>
                </div>        
            </div>
            <div class="row">
                <div class="col-md-5"><?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary', 'style' => 'border-radius:0px;']) ?></div>
                <div class="col-md-7 text-right" style="padding-top: 5px;"><?= Html::a('I remember password', ['site/login'], ['name' => 'login-button', 'class' => 'text-center']) ?></div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
