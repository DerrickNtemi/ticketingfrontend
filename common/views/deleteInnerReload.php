<?php 
use yii\helpers\Html;
?>
<p><b>Are you sure you want to delete this record ? </b></p>
<p id="reload-div" class="hidden"></p>
<p id="delete-url" class="hidden"></p>
<p id="success-div" class="hidden"></p>
<h2></h2>
<div id="progress-spinner" class="text-center" style="display: none;">
    <?= Html::img('@web/images/ajax-loader.gif', ['alt' => 'form processing']) ?>
</div>
<div class="form-group">
    <div class="alert alert-danger"  id='error-summary' role="alert" style="display: none;"> </div>
</div>
<div class="clearfix"></div>