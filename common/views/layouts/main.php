<?php
/* @var $this \yii\web\View */
/* @var $content string */

use common\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$identity = Yii::$app->user->getIdentity();
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <div class="row" id="header-container">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">                    
                    <div class="row" id="main-header">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div class="row">
                                <div class="col-xs-8 col-sm-8 col-md-3 col-lg-3" id="logo-holder">
                                    <?= Html::img("@web/images/logo.png", ['class' => 'img-responsive']) ?>
                                </div>
                                <div class="col-xs-1 col-sm-1 col-md-9 col-lg-9 header-title" >
                                    <p id="app-name"><?= $this->blocks['appName'] ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1"></div>
                    </div>
                    <div class ="row" id="row-header">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">                            
                            <div class="row">
                                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
                                    <div class="row">
                                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3"  id="semi-logo-holder"></div>
                                    </div>
                                </div>
                                <div class="col-xs-11 col-sm-11 col-md-11 col-lg-11 text-right notification-bar">
                                    <?php if (!Yii::$app->user->isGuest) : ?>
                                        <span class="glyphicon glyphicon-user"></span> 
                                        <?= $identity->name ?> 
                                        <?= Html::a("<span class='glyphicon glyphicon-transfer logout-icon'></span>", Url::toRoute(['site/change-password']), ['title' => 'Change password']) ?>
                                        <?= Html::a("<span class='glyphicon glyphicon-log-out logout-icon'></span>", Url::toRoute(['site/logout']), ['title' => 'Logout']) ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>                   
                    <div class="row" id="lower-row-header">
                        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                            <div class="row lower-header-row">
                                <div class="col-xs-8 col-sm-8 col-md-3 col-lg-3" id="lower-logo-holder"></div>
                                <div class="col-xs-2 col-sm-2 col-md-9 col-lg-9" id="lower-logo-holder-2"></div>
                            </div>
                        </div>
                        <div class="col-md-1 col-lg-1"></div>
                    </div>
                </div>
            </div>
            <div class="row" id="main-container">
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" id="main-content-div">
                    <?php if (Yii::$app->user->isGuest) : ?>
                        <h1 style="margin-top: 50px;"></h1>
                        <?= $content ?>
                    <?php else : ?>
                        <div class="row" style="min-height: 500px;">
                            <div class="col-md-3 col-lg-3 content-menu resizableHeight"> 
                                <?= $this->blocks['menu'] ?>
                            </div>
                            <div class="col-md-9 col-lg-9 resizableHeight">     
                                <?=
                                Breadcrumbs::widget([
                                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : ['label' => 'Home'],
                                    'homeLink' => isset($this->params['breadcrumbs']) ? [
                                        'label' => Yii::t('yii', Yii::t('app', 'Home')),
                                        'url' => Yii::$app->homeUrl,
                                            ] : false,
                                    'options' => ['class' => 'breadcrumb']
                                ])
                                ?>
                                <?= $content ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
            </div>

            <div class = "row" id = "footer-container">

                <div class = "col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                <div class = "col-xs-10 col-sm-10 col-md-10 col-lg-10">
                    <div class = "row" id = "row-footer">
                        <p class = "pull-left"></p>
                    </div>
                    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>
                </div>

            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
