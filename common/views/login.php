<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use kartik\form\ActiveForm;

$this->title = ' Login';
//$this->params['breadc rumbs'][] = $this->title;
?>
<div class="site-login" style="margin-bottom: 50px;">


    <div class="row" >
        <div id="login-container" style="float: none;">
            <h1 align="center" id="form-title">GET CONNECTED TO <?= $appName . " ACCOUNT " ?></h1>
            <?php if (Yii::$app->session->hasFlash('fail-message')): ?>
                <div class="alert alert-danger"><?= Yii::$app->session->getFlash('fail-message') ?></div>
            <?php endif; ?>
            <?php if (Yii::$app->session->hasFlash('success-message')): ?>
                <div class="alert alert-success"><?= Yii::$app->session->getFlash('success-message') ?></div>
            <?php endif; ?>
            <?php
            $form = ActiveForm::begin(['id' => 'login-form']);
            if ($model->hasErrors()) {
                echo $form->errorSummary($model, ['header' => '']);
            }
            ?>
            <div class="row" id="username-row">
                <div class="col-md-12 username-row">
                    <?= $form->field($model, 'username', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-user"></span>']], 'inputOptions' => ['placeholder' => 'username']])->textInput(['autofocus' => true])->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 password-row">
                    <?= $form->field($model, 'password', ['addon' => ['prepend' => ['content' => '<span class="glyphicon glyphicon-lock"></span>']], 'inputOptions' => ['placeholder' => 'password']])->passwordInput()->label(false) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button', 'style' => 'width:100%;']) ?>
            </div>
            <?php if ($appName == "Merchant") : ?>
                <div class="text-center">OR</div>
                <div class="form-group">
                    <?= Html::a('Create Account', ['site/account-creation'] , ['class' => 'btn btn-success', 'name' => 'login-button', 'style' => 'width:100%;']) ?>
                </div>
                <h1></h1>
            <?php endif; ?> 
            <div id="login-help">
                    <?= Html::a('Forgot Password', ['site/request-password-reset'] , ['name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
