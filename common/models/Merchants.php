<?php

namespace common\models;

class Merchants extends \yii\base\Model {

    public $id;
    public $businessName;
    public $email;
    public $businessAddress;
    public $town;
    public $countryId;
    public $phoneNumber;
    public $businessUrl;
    public $contactPersonFirstName;
    public $contactPersonMiddleName;
    public $contactPersonLastName;
    public $contactPersonEmail;
    public $contactPersonPhoneNumber;
    public $contactPersonPosition;
    public $createdAt;
    public $statusId;
    public $verifiedBy;
    public $verifiedAt;
    public $remarks;

    public function rules() {
        return [
            [['town','countryId','phoneNumber','contactPersonPhoneNumber','contactPersonEmail','contactPersonLastName', 'contactPersonFirstName',  'businessName', 'email'], 'required'],
            [['contactPersonPosition', 'contactPersonMiddleName', 'businessAddress', 'id','createdAt','statusId','verifiedBy','verifiedAt','remarks', 'businessUrl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'businessName' => 'Business Name',
            'email' => 'Email',
            'businessAddress' => 'Business Address',
            'town' => 'Town/City',
            'countryId' => 'Country',
            'phoneNumber'=>'Phone Number',
            'contactPersonEmail' => 'Email',
            'contactPersonPhoneNumber' => 'Phone Number',
            'businessUrl' => 'Business Url',
            'contactPersonFirstName' => 'First Name',
            'contactPersonMiddleName' => 'Middle Name',
            'contactPersonLastName' => 'Last Name',
            'contactPersonPhone' => 'Phone Number', 
            'contactPersonPosition' => 'Position',            
            'statusId' => 'Status',
            'createdAt' => 'Timestamp',
            'verifiedBy' => 'Verified By',
            'verifiedAt' => 'Verified At',
            'verifiedAt' => 'Remarks',
        ];
    }

}
