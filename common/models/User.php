<?php

namespace common\models;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $name;
    public $statusId;
    public $lastLogin;
    public $password;  
    public $loginAttempts;
    public $createdAt;
    public $email;
    public $phoneNumber;
    public $createdBy;
    public $userTypesId;
    public $userRolesId;
    public $merchantId;
    public $verifiedAt;
    public $authKey;
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $service = new Service;
        $result = $service->getUserById($id);
        return new static($result); 
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username, $password)
    {
        $service = new Service;
        $result = $service->login($username,$password);
        if (isset($result['item']) && !empty($result['item'])) {
            return new static(['id'=>$result['item']]);
        }
        return null;
    }
    
      

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

 

    public function getId() {
        return $this->id;
    }

}
