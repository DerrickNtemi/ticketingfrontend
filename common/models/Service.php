<?php

namespace common\models;

use Yii;
use linslin\yii2\curl;
use yii\helpers\Json;
use yii\web\HttpException;

class Service {

    public $createdBy;
    public function __construct() {
        $identity = Yii::$app->user->getIdentity();
        if ($identity) {
            $this->createdBy = $identity->id;
        }
        $this->appKey = Yii::$app->params['appKey'];
        $this->serviceUrl = Yii::$app->params['serviceUrl'];
        $this->apiKey = Yii::$app->params['apiKey'];
    }

    function createUserAccount($model) {
        $curl = new curl\Curl();
        $model->createdBy = $this->createdBy;
        $modelData = $model->getAttributes();
        unset($modelData['confirmPassword']);
        $data = Json::encode($modelData, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/users");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function deleteUser($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id, 'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/users");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    function merchantAccountCreation($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/merchants");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    function merchantAccountVerification($model,$action=1) {
        $model->verifiedBy = $this->createdBy;
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/merchants/verify?action=$action");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    function merchantAccountUpdating($model) {
        $model->verifiedBy = $this->createdBy;
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/merchants/update");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    function merchantAccountDeletion($model) {
        $model->verifiedBy = $this->createdBy;
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/merchants");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function activateAccount($token) {
        $curl = new curl\Curl();
        $data = Json::encode(['token' => $token], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/token");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    function requestPasswordReset($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/users/requestpasswordreset");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    

    //Login User
    function login($username, $password) {
        $curl = new curl\Curl();
        $data = Json::encode(['email' => $username, 'password' => $password], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . '/users/login');
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //Login User
    function getUserById($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/users/$id");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //reset Password
    function resetPassword($params) {
        $curl = new curl\Curl();
        $data = Json::encode($params, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/users/resetpassword");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //change Password
    function changePassword($model) {
        $curl = new curl\Curl();
        $model->id = $this->createdBy;
        $data = Json::encode(['id'=>$model->id,'currentPassword'=>$model->currentPassword,'newPassword'=>$model->newPassword], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/users/changepassword");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //get user roles
    function getRoles() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/roles");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //get users
    function getAllUsers($merchantId="",$statusId=1) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/users?merchantId=$merchantId&statusId=$statusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //get provinces
    function getProvinces() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/province");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //get business categories
    function getBusinessCategories() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/businesscategory");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
	

    public function javaToPhpTimestampConversion($timestamp) {
        return intval($timestamp / 1000);
    }

    public function timestampConversion($timestamp) {
        return strtotime(date("Y-m-d", $this->javaToPhpTimestampConversion($timestamp)));
    }

    public function timestampToString($timestamp) {
        return date("Y-m-d", $this->javaToPhpTimestampConversion($timestamp));
    }

    public function timestampToDateTime($timestamp) {
        return date("Y-m-d H:i:s", $this->javaToPhpTimestampConversion($timestamp));
    }

    public function timestampToString1($timestamp) {
        return date("Y-m-d H:i:s", $this->javaToPhpTimestampConversion($timestamp));
    }
    
    
    //get merchants
    function getMerchants($statusId="") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/merchants?statusId=$statusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get ticketnames
    function getTicketNames($statusId="") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/ticketname/$this->createdBy?statusId=$statusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get merchant pending events
    function getMerchantPendingEvent($statusId = ""){
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/pending/$this->createdBy?statusId=$statusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get merchant events
    function getMerchantEvents($statusId = ""){
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/$this->createdBy?statusId=$statusId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get events
    function getEvents($statusId = ""){
        $curl = new curl\Curl();
		
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events?statusId=$statusId");
        //print_r($response); exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get active events
    function getActiveEvents(){
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/active");
        //print_r($response); exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get ticketname
    function getTicketName($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/ticketname/$id/$this->createdBy");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get eventTypes
    function getEventTypes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventtype");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get pending event
    function getPendingEvent() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/pending/$this->createdBy");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event ticket tiers
    function getEventTicketTiers($eventId,$statuId="") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventtickets/all/$eventId/$this->createdBy?statusId=$statuId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event galleries
    function getEventGalleries($eventId,$merchantId,$statuId="") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventgallery/all/$eventId/$merchantId?statusId=$statuId");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event sponsors
    function getEventSponsors($eventId,$merchantId,$statuId="") {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventsponsor/all/$eventId/$merchantId?statusId=$statuId");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event sponsor
    function getEventSponsor($id,$merchantId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventsponsor/$id/$merchantId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event gallery
    function getEventGallery($id,$merchantId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/eventgallery/$id/$merchantId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    
    //get event details
    function getEventDetails($eventId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/comprehensive/$eventId/$this->createdBy");

        if ($curl->responseCode == 200) {
            //print_r($response); exit;
            return Json::decode($response);

        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get event details comprehensive - without merchant id
    function getEventComprehensive($eventId) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/comprehensive/$eventId");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //get single event
    function getSingleEvent($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/events/$id/$this->createdBy");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get counties
    function getCounty() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/county");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get ticket types
    function getTicketTypes() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/tickettype");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //get countries
    function getCountry() {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/country");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to create ticket name
    function createTicketName($model) {
        $model->createdBy = $this->createdBy;
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->post($this->serviceUrl . "/ticketname");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to delete ticket name
    function ticketNameDeletion($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/ticketname");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    
    //get merchant
    function getMerchant($id) {
        $curl = new curl\Curl();
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "App-Key: $this->appKey",
                    "Api-Key: $this->apiKey",
                ])
                ->get($this->serviceUrl . "/merchants/$id");
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }   
    
    //function to create event details
    function createEventDetails($model) {
        $id = $model->id;
        $model->createdBy = $this->createdBy;
        $curl = new curl\Curl();
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['statusId']);
        unset($modelAttributes['endDateTime']);
        unset($modelAttributes['startDateTime']);
        unset($modelAttributes['createdAt']);
        unset($modelAttributes['merchantId']);
        unset($modelAttributes['commission']);
        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
                
        $response = empty($id) ? $curl->post($this->serviceUrl . "/events") :  $curl->put($this->serviceUrl . "/events") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to create ticket tier
    function createTicketTier($model) {
        $id = $model->id;
        $model->createdBy = $this->createdBy;
        $modelAttributes = $model->getAttributes();
        unset($modelAttributes['statusId']);
        unset($modelAttributes['validTill']);
        unset($modelAttributes['remainingTickets']);
        unset($modelAttributes['createdAt']);
        unset($modelAttributes['merchantId']);
        $curl = new curl\Curl();
        $data = Json::encode($modelAttributes, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
                
        $response = empty($id) ? $curl->post($this->serviceUrl . "/eventtickets") :  $curl->put($this->serviceUrl . "/eventtickets") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to create event sponsor
    function createEventSponsor($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
                
        $response = empty($model->id) ? $curl->post($this->serviceUrl . "/eventsponsor") :  $curl->put($this->serviceUrl . "/eventsponsor") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

    //function to create event gallery
    function createEventGallery($model) {
        $curl = new curl\Curl();
        $data = Json::encode($model, JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data);
                
        $response = empty($model->id) ? $curl->post($this->serviceUrl . "/eventgallery") :  $curl->put($this->serviceUrl . "/eventgallery") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to delete ticket tier
    function deleteTicketTier($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response =  $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/eventtickets") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to delete event sponsor
    function deleteEventSponsors($id,$eventId,$merchantId) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'eventId'=>$eventId, 'merchantId'=>$merchantId], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response =  $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/eventsponsor") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to delete event gallery
    function deleteEventGallery($id,$eventId,$merchantId) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'eventId'=>$eventId, 'merchantId'=>$merchantId], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response =  $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/eventgallery") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to delete ticket tier
    function changeTicketUnits($id, $units) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy,'remainingTickets'=>$units], JSON_NUMERIC_CHECK);
        //echo $data; exit;
        $response =  $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/eventtickets") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to complete event
    function completeEventCreation($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response =  $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/events/complete") ;
        //echo $response; exit;
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    //function to delete event
    function deleteEvent($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->delete($this->serviceUrl . "/events");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    
    //function to authorize event
    function authorizeEvent($id) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/events/authorize");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    //function to publish event
    function publishEvent($id,$commission) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'commission'=>$commission,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/events/publish");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }
    //function to cancel event
    function cancelEvent($id,$remarks) {
        $curl = new curl\Curl();
        $data = Json::encode(['id'=>$id,'remarks'=>$remarks,'createdBy'=>$this->createdBy], JSON_NUMERIC_CHECK);
        $response = $curl->setOption(
                        CURLOPT_HTTPHEADER, [
                    'Content-Type:application/json',
                    'Accept:application/json',
                    "app-key: $this->appKey",
                    "api-key: $this->apiKey",
                ])
                ->setOption(CURLOPT_POSTFIELDS, $data)
                ->put($this->serviceUrl . "/events/cancel");
        if ($curl->responseCode == 200) {
            return Json::decode($response);
        } else {
            throw new HttpException($curl->responseCode, 'An error occurred');
        }
    }

	/* CONSUMER SERVICES */
}
