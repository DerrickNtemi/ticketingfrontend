<?php

namespace common\models;

class Events extends \yii\base\Model {

    public $id;
    public $name;
    public $description;
    public $poster;
    public $eventTypeId;
    public $venue;
    public $location;
    public $countyId;
    public $countryId;
    public $startDate;
    public $startTime;
    public $endDate;
    public $endTime;
    public $startDateTime;
    public $endDateTime;
    public $statusId;
    public $createdBy;
    public $createdAt;
    public $merchantId;
    public $commission;
    public $latitude;
    public $longitude;
    public $remarks;

    public function rules() {
        return [
            [['name','description','poster','eventTypeId','venue','location','countryId','startDate','startTime','endDate','endTime'], 'required'],
            [['id','countyId','createdAt','statusId','createdBy','merchantId','startDateTime','endDateTime','commission','latitude','longitude','remarks'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Event Name',
            'description' => 'Event Description',
            'poster' => 'Event Poster',
            'eventTypeId' => 'Event Type',
            'venue' => 'Venue',
            'location' => 'Town/City',
            'countyId' => 'County',
            'countryId' => 'Country',
            'startDate' => 'Start Date',
            'startTime' => 'Start Time',
            'endDate' => 'End Date',
            'endTime' => 'End Time',
            'commision'=>'Commission',
            'longitude' => 'Longitude',
            'latitude' => 'Latitude',
            'remarks'=>'Remarks',
        ];
    }

}
