<?php

namespace common\models;

class ResetPasswordRequest extends \yii\base\Model {
    public $username;

    public function rules() {
        return [
            [['username'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'username' => 'Username',
        ];
    }

}
