<?php

namespace common\models;

class EventTickets extends \yii\base\Model {

    public $id;
    public $eventsId;
    public $ticketTypeId;
    public $ticketNameId;
    public $price;
    public $validTill;
    public $ticketQuantity;
    public $remainingTickets;
    public $purchaseStartDateTime;
    public $purchaseEndDateTime;
    public $merchantId;
    public $statusId;
    public $createdBy;
    public $createdAt;

    public function rules() {
        return [
            [['eventsId','ticketTypeId','ticketNameId','price','ticketQuantity'], 'required'],
            [['id','createdAt','statusId','createdBy','merchantId','validTill','remainingTickets','purchaseStartDateTime','purchaseEndDateTime'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'eventsId' => 'Event',
            'ticketTypeId' => 'Ticket Type',
            'ticketNameId' => 'Ticket Name',
            'price' => 'Ticket Price',
            'validTill' => 'Valid Till',
            'ticketQuantity' => 'Ticket Units',
            'purchaseStartDateTime' => 'Start Date / Time',
            'purchaseEndDateTime' => 'End Date / Time',
        ];
    }

}
