<?php

namespace common\models;

class EventSponsor extends \yii\base\Model {

    public $id;
    public $eventId;
    public $name;
    public $itemSponsoring;
    public $logo;
    public $businessUrl;
    public $merchantId;
    public $statusId;
    public $createdAt;

    public function rules() {
        return [
            [['name','logo'], 'required'],
            [['id','eventId','itemSponsoring','createdAt','statusId','merchantId','businessUrl'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'eventId' => 'Event',
            'name' => 'Sponsor Name',
            'logo' => 'Sponsor Logo',
            'itemSponsoring' => 'Item Sponsoring',
            'businessUrl'=>'Business Url',
        ];
    }

}
