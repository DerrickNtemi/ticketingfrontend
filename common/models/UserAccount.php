<?php

namespace common\models;

class UserAccount  extends \yii\base\Model {
    public $id;
    public $name;
    public $email;
    public $phoneNumber;
    public $password; 
    public $confirmPassword; 
    public $userTypesId;
    public $userRolesId;
    public $createdAt;
    public $createdBy;
    public $statusId;
    public $verifiedAt;
    public $lastLogin; 
    public $loginAttempts;
    public $merchantId;

    public function rules() {
        return [
            [['name','email','phoneNumber','password','confirmPassword','userTypesId','userRolesId'], 'required'],
            [['userTypesId','userRolesId'], 'integer'],
            [['id','createdAt','statusId','createdBy','merchantId','verifiedAt','lastLogin','loginAttempts'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Name',
            'email' => 'Username',
            'phoneNumber' => 'Phone Number',
            'password' => 'Password',
            'confirmPassword' => 'Confirm Password',
            'userTypesId' => 'User Type',
            'userRolesId' => 'User Role'
        ];
    }
    
    
    
}
