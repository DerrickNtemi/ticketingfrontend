<?php

namespace common\models;

class ResetPassword extends \yii\base\Model {

    public $id;
    public $password;
    public $passwordRepeat;

    public function rules() {
        return [
            [['password', 'passwordRepeat'], 'required'],
            [['id'], 'safe'],
            ['passwordRepeat', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'password' => 'New Password',
            'passwordRepeat' => 'Confirm Password'
        ];
    }

}
