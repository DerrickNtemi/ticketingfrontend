<?php

namespace common\models;

class ChangePassword  extends \yii\base\Model
{
    public $id;
    public $currentPassword;  
    public $newPassword;
    public $confirmPassword;
    
    public function rules() {
        return [
            [['currentPassword', 'newPassword','confirmPassword'], 'required'],
            [['id'], 'safe'],
            ['confirmPassword', 'compare', 'compareAttribute'=>'newPassword', 'message'=>"Passwords don't match" ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'currentPassword' => 'Current Password',
            'newPassword' => 'New Password',
            'confirmPassword' => 'Confirm Password'
        ];
    }
}
