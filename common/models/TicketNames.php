<?php

namespace common\models;

class TicketNames extends \yii\base\Model {

    public $id;
    public $name;
    public $createdAt;
    public $statusId;
    public $createdBy;
    public $merchantId;

    public function rules() {
        return [
            [['name'], 'required'],
            [['id','createdAt','statusId','createdBy','merchantId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'name' => 'Ticket Name',
        ];
    }

}
