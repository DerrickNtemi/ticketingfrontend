<?php

namespace common\models;

class EventGallery extends \yii\base\Model {

    public $id;
    public $eventId;
    public $photo;
    public $title;
    public $description;
    public $merchantId;
    public $statusId;
    public $createdAt;

    public function rules() {
        return [
            [['photo','title','description'], 'required'],
            [['id','eventId','createdAt','statusId','merchantId'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'eventId' => 'Event',
            'photo' => 'Photo',
            'title' => 'Title',
            'description' => 'Description',
        ];
    }

}
